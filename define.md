## Parameter definition language

The `define.mk` implements declarative DSL, which targets simplify parameter definition.
You define parameters in format like ordinary config/ini files, which used in GIT. The DSL compiler transforms your definition into C header with definition and implementation of parameters. You simply include it into your project to work with parameters.

### Top-level structure

At the top level each parameter defined as config group:

    [param "name_of_first_param"]
    first_field_of_param = first_field_value
    ...
    last_field_of_param = last_field_value
    ...
    [param "name_of_last_param"]
    first_field_of_param = first_field_value
    ...
    last_field_of_param = last_field_value

The name is C identifier.

### The parameters and groups

Each valid parameter must have the `type` field. The parameters without `type` field will be treated as groups of parameters. For example:

    [param "sensors"]
    info = The sensors

    [param "temperature"]
    type = real
    info = The temperature sensor value

    [param "pressure"]
    type = real
    info = The pressure sensor value

    [param "handlers"]
    info = The handlers

    [param "heater"]
    type = uint
    info = The heater power value

    [param "pump"]
    type = uint
    info = The pump power value

We defined two groups of parameters:

1. The first group named "sensors" and have two parameters: "temperature" and "pressure".
2. The second group named "handlers" and have two parameters: "heater" and "pump".

### Basic rules of parameter definition

#### The types of parameters

All types is divided on two classes:

* Atomic types, like `uint`, `sint`, `real`, `enum`
* Compound types, like `cstr`, `hbin`, `ipv4`, `mac`

#### The size of value

The `size` field for atomic types means the number of bits in value, but for compound types it means the number of bytes. Usually you required to provide this field.

#### The value binding

In some cases you may want to bind parameter with data, which already defined in other place. You can to use `bind` field for that purpose. The value of this field will be treated as variable name. If value is empty, the name of parameter will be used as variable name.

See example below:

    [param "water_level"]
    type=uint
    size=8
    info=The actual level of water in container
    max=100
    getable
    bind

    [param "water_temperature"]
    type=sint
    size=8
    info=The actual temperature of water in container
    min=-50
    max=100
    getable
    bind=water_temp

In example, we have two parameters:

1. The `water_level`, which linked with external `uint8_t water_level;`
2. The `water_temperature`, which linked with external `int8_t water_temp;`

#### The getter and setter

The `get` and `set` fields turns parameter to virtual. This means that getter and setter will be used instead of accessing to value of parameter using pointer to data cell.
If value of `get` or `set` field is empty, then names will be look like `<name>_get` or `<name>_set`.

Don't forget define getter and setter functions in your side.

#### Parameter flags

Some fields without value called a flags:

* The flag `getable` means that application can get value of parameter, by default application cannot do this.
* The flag `setable` means that application can set value of parameter, by default application cannot do this.
* The flag `persist` means that value of parameter will be stored in persistent storage for restoring on every initialization, by default the value isn't storable.

#### Providing value for vitrual parameters

Usually the virtual parameters don't need to have variable, which associated with it, but in some cases it isn't that you need. Use `data` flag to override default behavior.

### The options for enums

To define available values for enum you can use sections with type `value`. See example:

    [param "mode"]
    type=enum
    size=8
    info=Control mode
    def=mode_auto
    getable
    setable

    [value "mode.off"]
    val=mode_off
    info=Disabled

    [value "mode.auto"]
    val=mode_auto
    info=Automatic

    [value "mode.on"]
    val=mode_manual
    info=Manual

In example above we have parameter `mode` with `enum` type. This enum have three available values: `off`, `on`, `auto`.

#### Localization

All fields may have variants, which translated to any native languages. To do this you can use language suffixes. The translated field would looks like `<lield-name>-<language-code>`. For example:

    [param "net"]
    info=The network
    info-ru=Сеть

    [param "net_gw"]
    info=The gateway
    info-ru=Шлюз
    type=ipv4
    getable
    setable
    def=192.168.10.1

    [param "net_name"]
    info=The hostname
    info-ru=Имя хоста
    type=cstr
    size=16
    getable
    setable
    def=example.com
    def-us=example.us
    def-ru=example.ru

### How to generate definition

To generate definition of parameters as C-header, you need to include `define.mk` in your `Makefile` and call user function `param-make` with next arguments:

1. Input file, which contain definition of your parameters.
2. Output file, which is a target C-header to generate.
3. Namespace, which will be used as prefix.
4. Language code to generate localized definition.

The last two fields is optional and may be omitted. The name of input file will be used as default namespace.

This function will add target with name of output file, which depended from input file.

See example below:

    include $(libparam.path)/define.mk

    # Generate single definition
    $(call param-make,params.x,include/params.h)

    # Different definitions for languages
    $(for lang,$(langs),\
      $(call param-make,params.x,include/params-$(lang).h,,$(lang)))

### How to use generated definition

To use generated definition in your code, you heed to include generated C-header.

There are two different usecases:

* Accessing to declaration
* Providing implementation

In first case, you usually need to access to your parameters using API from different places of our code.
In second case, you need to have implementation of parameters in some place of your application.

See example:

    #include "param.h"
    #include "params.h" /* Include declaration (first case) */

    /* our definitions */

    #define __params_c__ /* define this to add implementation */
    #include "params.h" /* Include implementation (second case) */

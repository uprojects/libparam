Overview
========

Tiny footprint library for controlling hardware devices.
The main goal of this library is providing common interface for hardware devices using some set of parameters to control it.

Parameters
----------

Each parameter represented into system using descriptor. The descriptor contains some set of fields:

* Unique id
* Flags and options:
    - Access mode
    - Use-case flags
    - Value type
    - Used constraints
    - Used text fields
* Value size
* Pointer to value or getter/setter
* Pointer to constraints
* Pointer to text fields

The access mode flags:

* Getable (Read-only value)
* Setable (Write-only value)

The use-case options:

* Persist (Value can be saved to and loaded from eeprom or flash memory)
* Virtual (Getter/Setter functions will be used for accessing to value instead of direct access by pointer)

The value types:

* enum
* uint
* sint
* real
* cstr
* hbin

Constraints:

* Default value
* Minimal value
* Maximal value
* Changing step

Text fields:

* Symbolic name
* Description
* User hint
* Menu item

Collections
-----------

The several parameters may be grouped in collections to provide common access to it including reading descriptors by host.

Interface
---------

The library implements simple binary interface to access to parameters, which allows:

* Get parameter descriptors
* Get parameter values
* Set parameter values

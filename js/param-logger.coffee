#!/usr/bin/env coffee

{Client} = require "./param"

program = "logger"

usage = ->
  console.log """
  Usage: #{program} <device> <period> <param1>[... <paramN>]
  Log device data to processing and/or plotting using tools like GNUPlot.
  """
  process.exit 1

[coffee, script, device, period, params...] = process.argv

do usage unless device and period and params.length > 0

client = new Client device

failed = (error)->
  throw error

schedule = (time = do Date.now)->
  setTimeout retrieve, period + time - do Date.now

initial = null

retrieve = ->
  time = do Date.now
  initial = time unless initial?
  client.getValues params, (values)->
    #values[index] = 'NaN' for value, index in values when not value?
    console.log 0.001 * (time - initial), values...
    schedule time

client.connect retrieve, failed

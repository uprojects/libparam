#!/usr/bin/env coffee

{Client} = require "./param"

program = "param-tool"

usage = ->
  console.log """
  Usage: #{program} <device> [<command> <args...>]
    <device> The device path or connection URI
    <command> The single command to execute
    <args...> The arguments of single command
  Examples:
    # Open local console on ttyUSB0 device:
    #{program} /dev/ttyUSB0
    # Open remote console on port 23 of host.dom:
    #{program} telnet://host.dom
    # Get list of parameters:
    #{program} <uri> info
    # Get parameter's description:
    #{program} <uri> info paramX
    # Get values of parameters:
    #{program} <uri> gets paramX paramY paramZ
    # Set values of parameters:
    #{program} <uri> sets paramX valueX paramY valueY paramZ valueZ
  """
  process.exit 1

[coffee, script, device, command...] = process.argv

do usage unless device

client = new Client device

runCommand = ([command, args...], done)->
  error = (error)->
    console.error error
    do done
  args = (arg for arg in args or [] when arg)
  switch command
    when "info"
      if args.length > 0
        client.getDescs args, (descs)->
          for name, desc of descs
            console.log "#{name}:"
            for key, val of desc
              if "object" isnt typeof val
                console.log "  #{key}: #{val}"
              else
                console.log "  #{key}:"
                if val instanceof Array
                  for val_ in val
                    console.log "    - #{val_}"
                else
                  for key_, val_ of val
                    console.log "    #{key_}: #{val_}"
          do done
        , error
      else
        client.getParams (params)->
          console.log params.join " "
          do done
        , error
    when "gets"
      client.getValues args, (values)->
        console.log (JSON.stringify value for value in values).join " "
        do done
      , error
    when "sets"
      vals = {}
      ok = no
      while args.length > 0
        [param, value, args...] = args
        if param? and value?
          vals[param] = value
          ok = yes
      if ok?
        client.setValues vals, ->
          do done
        , error
      else
        do done
    else
      console.log "Invalid command: #{command}"
      do done

commands = [
  "info"
  "gets"
  "sets"
  "quit"
]

params = null

completeList = (line, list, pref = "")->
  if line.length > 0
    #console.log "[%s]", ("#{item} " for item in list when line.length < item.length + 2 and 0 is item.indexOf line)
    "#{pref}#{item} " for item in list when line.length < item.length + 2 and 0 is item.indexOf line
  else
    "#{pref}#{item} " for item in list

completeDone = (line, list, done)->
  comp = completeList line, list
  if comp.length < 1
    done null, [[], line]
  if comp.length < 2
    done null, [[], line]

complete = (line, done)->
  if -1 is line.indexOf " " # has incomplete command
    done null, [(completeList line, commands), line]
  else # has complete command and space
    [command, args...] = line.split /\s+/g
    if command in commands and command isnt "sets" or 1 is args.length % 2
      completeParams = ->
        [args..., last] = args
        #args.push ""
        #"#{command} #{args.join " "}"
        done null, [(completeList last, params), last]
      if params?
        do completeParams
      else
        client.getParams (params_)->
          params = params_
          do completeParams
    else
      done null, [[], line]

client.connect ->
  if command.length > 0
    runCommand command, ->
      process.exit 0
  else
    {createInterface} = require "readline"
    rl = createInterface
      input: process.stdin
      output: process.stdout
      completer: complete
    rl.setPrompt "> "
    rl.on "line", (line)->
      command = [name, args...] = line.split /\s+/g
      if name
        if name is "quit"
          do rl.close
          process.exit 0
        else
          do rl.pause
          runCommand command, ->
            do rl.resume
            do rl.prompt
      else
        do rl.prompt
    do rl.prompt

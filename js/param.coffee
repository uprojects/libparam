{Transform} = require "stream"
{StringDecoder} = require "string_decoder"
{parse} = require "url"

class JSONStringifier extends Transform
  constructor: ->
    super writableObjectMode: yes
    @_writableState.objectMode = yes
  
  _transform: (chunk, encoding, done)->
    try
      data = JSON.stringify chunk
    catch err
      @emit "error", err
    @push data if data?
    do done

class JSONParser extends Transform
  constructor: ->
    super readableObjectMode: yes
    @_readableState.objectMode = yes
    @_buffer = ""
    @_decoder = new StringDecoder "utf8"
  
  _transform: (chunk, encoding, done)->
    @_buffer += @_decoder.write chunk
    hasData = no
    try
      data = JSON.parse @_buffer
      hasData = yes
    if hasData
      @_buffer = ""
      @push data
    do done

class @Client
  constructor: (uri = "/dev/ttyUSB0", @timeout = 1000)->
    {hostname: host, port, pathname: path, hash} = parse uri
    @options = if host?
      port ?= 23
      {host, port}
    else if path?
      {path, hash}
  
  connect: (result, error)->
    switch
      when /^\/dev\/tty/.test @options.path
        @serialConnect result, error
      else
        @networkConnect result, error

  serialConnect: (result, error)->
    SerialPort = require "serialport"
    @client = new SerialPort @options.path,
      autoOpen: no
      baudRate: (@options.hash.slice 1)|0 if /^#[0-9]{1,5}$/.test @options.hash

    @input = new JSONStringifier
    @output = new JSONParser

    @input.pipe @client
    @client.pipe @output

    @client.once "open", result
    @client.once "error", error if error

    do @client.open

  networkConnect: (result, error)->
    {createConnection} = require "net"
    @client = createConnection @options

    @input = new JSONStringifier
    @output = new JSONParser

    @input.pipe @client
    @client.pipe @output

    @client.once "connect", result
    @client.once "error", error if error
  
  runCommand: (command, result, error)->
    timer = setTimeout (-> error? new Error "Timeout reached"), @timeout
    @output.once "data", (response)->
      clearTimeout timer
      if response?.error
        error? new Error response.error
      else
        result? response
    @input.write command
  
  getParams: (result, error)->
    @runCommand info: null, result, error
  
  isStrList = (list)-> (Array.isArray list) and 0 is (0 for item in list when "string" isnt typeof item).length
  
  isValDict = (dict)-> ("object" is typeof dict) and not (Array.isArray dict) and 0 is (0 for key, val of dict when val isnt null and "object" is typeof val).length
  
  getDescs: (params, result, error)->
    if isStrList params
      do ret = (params, descs = {})=>
        if params.length > 0
          [param, params...] = params
          @runCommand info: param, (desc)->
            descs[param] = desc
            ret params, descs
          , error
        else
          result descs
    else
      error? new Error "Invalid argument #{JSON.stringify params}"
  
  getValues: (params, result, error)->
    if isStrList params
      @runCommand gets: params, result, error
    else
      error? new Error "Invalid argument"

  setValues: (params, result, error)->
    if isValDict params
      @runCommand sets: params, result, error
    else
      error? new Error "Invalid argument"

#include "param.h"
#include "param-intern.h"

#if PARAM_NAME
typedef uintptr_t addr_t;

#ifndef PARAM_STORE
#define PARAM_STORE dummy
#endif /* PARAM_STORE */

#define PARAM_PERS_INCL2(backend) param-store-backend.h
#define PARAM_PERS_INCL1(backend) #backend
#define PARAM_PERS_INCL0(backend) PARAM_PERS_INCL1(backend)
#define PARAM_PERS_INCL(backend) PARAM_PERS_INCL0(PARAM_PERS_INCL2(backend))
#include PARAM_PERS_INCL(PARAM_STORE)

#if store_align == 1
typedef uint8_t store_atom_t;
#elif store_align == 2
typedef uint16_t store_atom_t;
#elif store_align == 4
typedef uint32_t store_atom_t;
#elif store_align == 8
typedef uint64_t store_atom_t;
#endif /* store_align */

static const store_atom_t dead_flag = 0;
static const param_uuid_t free_uuid = ~0;

typedef union {
  uint32_t cell;
  uint32_t flag;
  param_uuid_t uuid;
  param_size_t size;
  uint32_t data[0];
  uint8_t byte[0];
} cell_t;

/*
typedef struct {
  param_uuid_t uuid;
  store_atom_t flag;
  param_size_t size;
  store_atom_t data[0];
} store_cell_t;
*/

static inline param_uuid_t cell_uuid(addr_t addr) {
  cell_t cell;
  
  store_read(addr, &cell, sizeof(cell));
  
  return cell.uuid;
}

static inline param_size_t cell_size(addr_t addr) {
  cell_t cell;
  
  store_read(addr + sizeof(cell), &cell, sizeof(cell));
  
  return cell.size;
}

static inline char cell_dead(addr_t addr) {
  cell_t cell;
  
  store_read(addr + sizeof(cell) * 2, &cell, sizeof(cell));
  
  return cell.flag == dead_flag;
}

static inline addr_t cell_data(addr_t addr) {
  return addr + sizeof(cell_t) * 3;
}

#define align_size sizeof(cell_t)
#define align_mask (align_size - 1)

static inline addr_t addr_align(addr_t addr) {
  if (addr & align_mask)
    addr = (addr + align_size) & ~align_mask;
  
  return addr;
}

static inline addr_t cell_next(addr_t addr) {
  return addr_align(cell_data(addr) + cell_size(addr));
}

static void cell_kill(addr_t addr) {
  store_unlock();
  
  store_write(addr + sizeof(cell_t) * 2, (void*)&dead_flag, sizeof(dead_flag));
  
  store_lock();
}

static addr_t find_data_cell(param_uuid_t uuid) {
  addr_t addr = store_start;
  addr_t cell = store_end;
  
  for (; addr < store_end; ) {
    if (cell_uuid(addr) == free_uuid) break;
    if (cell_uuid(addr) == uuid && !cell_dead(addr)) {
      if (cell != store_end) {
        cell_kill(cell);
      }
      cell = addr;
    }
    addr = cell_next(addr);
  }
  
  return cell;
}

static addr_t find_free_cell(param_size_t size) {
  addr_t addr = store_start;
  
  for (; addr < store_end; ) {
    if (cell_uuid(addr) == free_uuid)
      return cell_data(addr) + size < store_end ? addr : store_end;
    addr = cell_next(addr);
  }
  
  return store_end;
}

int param_load(const param_desc_t *param) {
  if (param_flag(param) & param_persist) {
    addr_t addr = find_data_cell(param_uuid(param));
    if (addr != store_end) {
      uint16_t len = cell_size(addr);
      cell_t data[addr_align(len) / sizeof(cell_t)
#if PARAM_CSTR
                  /* some optimization for C-strings */
                  + param_type(param) == param_cstr ? 1 : 0
#endif
                  ];
      
      store_read(cell_data(addr), data, len);

#if PARAM_CSTR
      /* some optimization for C-strings */
      if (param_type(param) == param_cstr) {
        data->byte[len] = '\0';
      }
#endif
      
      return param_raw_set(param, data);
    }
  }
  
  return param_error;
}

static addr_t rewrite_storage(param_uuid_t self_uuid, addr_t addr) {
 start:
  if (cell_data(addr) >= store_end || cell_uuid(addr) == free_uuid) {
    /* when we complete read all parameters to stack
       we need to erase block before rewriting */
    store_erase(store_start);
    return store_start;
  }
  
  if (cell_uuid(addr) == self_uuid || cell_dead(addr)) {
    /* skip reading same or dead cell */
    addr = cell_next(addr);
    goto start;
  }
  
  cell_t cell = { .uuid = cell_uuid(addr) };
  param_size_t size = cell_size(addr);
  uint16_t len = addr_align(size);
  uint8_t data[len]; /* the parameter's value */
  
  /* read data from flash to stack */
  store_read(cell_data(addr), data, len);
  addr += len;
  
  /* recursion */
  addr = rewrite_storage(self_uuid, cell_next(addr));
  
  /* write uuid of saved value */
  store_write(addr, cell.data, sizeof(cell));
  cell.uuid = 0;
  addr += sizeof(cell);
  
  /* write size of saved value */
  cell.size = size;
  store_write(addr, cell.data, sizeof(cell));
  addr += sizeof(cell)
    /* skip write dead flag */
    + sizeof(cell);
  
  /* write param data */
  store_write(addr, (uint32_t*)data, len);
  addr += len;
  
  /* return the address to write the next parameter */
  return addr;
}

int param_save(const param_desc_t *param) {
  addr_t addr = find_free_cell(param_size(param));
  
  if (addr == store_end) {
    store_unlock();
    
    /* when we have no free space to write parameter
       we need to rewrite storage */
    addr = rewrite_storage(param_uuid(param), store_start);
    
    store_lock();
  } else {
    addr_t cell = find_data_cell(param_uuid(param));
    
    if (cell != store_end)
      cell_kill(cell);
  }

  param_size_t size = param_size(param);
  param_size_t len = addr_align(size);
  uint8_t data[len]; /* the parameter's value */

  /* fill zeros to end */
  memset(data, 0, len);
  
  /* read param value */
  param_raw_get(param, data);
  
#if PARAM_CSTR
  /* some optimization for C-strings */
  if (param_type(param) == param_cstr) {
    size = strlen((const char*)data);
    len = addr_align(size);
  }
#endif
  
  store_unlock();
  
  cell_t cell;
  
  /* write uuid of saved value */
  cell.uuid = param_uuid(param);
  store_write(addr, cell.data, sizeof(cell));
  cell.uuid = 0;
  addr += sizeof(cell);
  
  /* write size of saved value */
  cell.size = size;
  store_write(addr, cell.data, sizeof(cell));
  addr += sizeof(cell)
    /* skip write dead flag */
    + sizeof(cell_t);

  /* write param data */
  store_write(addr, (uint32_t*)data, len);
  addr += len;
  
  store_lock();
  
  return param_success;
}

#endif /* PARAM_NAME */

#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "param.h"
#include "elastr.h"
#include "param-estr.h"

int param_getestr_type(const param_desc_t *param, estr_t *str) {
  switch (param_type(param)) {
#define T(t) case param_##t: return estr_putsc(str, #t)
    T(none);
#if PARAM_ENUM
    T(enum);
#endif /* PARAM_ENUM */
#if PARAM_SINT
    T(sint);
#endif /* PARAM_SINT */
#if PARAM_UINT
    T(uint);
#endif /* PARAM_UINT */
#if PARAM_SFIX
    T(sfix);
#endif /* PARAM_SFIX */
#if PARAM_UFIX
    T(ufix);
#endif /* PARAM_UFIX */
#if PARAM_REAL
    T(real);
#endif /* PARAM_REAL */
#if PARAM_CSTR
    T(cstr);
#endif /* PARAM_CSTR */
#if PARAM_HBIN
    T(hbin);
#endif /* PARAM_HBIN */
#if PARAM_IPV4
    T(ipv4);
#endif /* PARAM_IPV4 */
#if PARAM_MAC
    T(mac);
#endif /* PARAM_MAC */
#undef T
  }
  return param_error;
}

static inline int estr_putsq_rom(estr_t *str, const char *nstr) {
  param_size_t len = PARAM_ROM_LEN(nstr) + 1;
  estr_extend(str, len);
  PARAM_ROM_READ(estr_str(str) + estr_len(str), nstr, len);
  estr_len(str) += len - 1;
  return len;
}

static int getstr(const param_desc_t *param, const param_cell_t *val, estr_t *str){
  if (val != NULL) {
    switch (param_type(param)) {
#if PARAM_ENUM
    case param_enum: {
#if PARAM_NAME && PARAM_TEXT /* symbolic enum */
      estr_uint_t index = 0, count = param_enum_count(param);
      switch (param_size(param)) {
#define ENUM_GET(type)                                             \
        case sizeof(type): {                                       \
          estr_uint_t value = PARAM_ROM_GET(*(const type*)val);    \
          for (; index < count; index++)                           \
            if (value == PARAM_ROM_GET(*(const type*)param_enum_value(param, index))) break; \
        } break
#if PARAM_8BIT
        ENUM_GET(uint8_t);
#endif /* PARAM_8BIT */
#if PARAM_16BIT
        ENUM_GET(uint16_t);
#endif /* PARAM_16BIT */
#if PARAM_32BIT
        ENUM_GET(uint32_t);
#endif /* PARAM_32BIT */
#if PARAM_64BIT
        ENUM_GET(uint64_t);
#endif /* PARAM_64BIT */
#undef ENUM_GET
      default: return param_error;
      }
      const char *_val = param_enum_text(param, param_none, index);
      if (_val != NULL) {
        return estr_putsq_rom(str, _val);
      } else {
        return 0;
      }
#else /* PARAM_NAME && PARAM_TEXT */ /* numeric enum */
      return 0;
#endif /* PARAM_NAME && PARAM_TEXT */
    }
#endif /* PARAM_ENUM */
#define NUM_GET(type) case sizeof(type): _val = PARAM_ROM_GET(*(const type*)val); break
#if PARAM_SINT
    case param_sint: {
      estr_sint_t _val = 0;
      switch (param_size(param)) {
#if PARAM_8BIT
        NUM_GET(int8_t);
#endif /* PARAM_8BIT */
#if PARAM_16BIT
        NUM_GET(int16_t);
#endif /* PARAM_16BIT */
#if PARAM_32BIT
        NUM_GET(int32_t);
#endif /* PARAM_32BIT */
#if PARAM_64BIT
        NUM_GET(int64_t);
#endif /* PARAM_64BIT */
      }
      return estr_putsd(str, _val);
    }
#endif /* PARAM_SINT */
#if PARAM_UINT
    case param_uint: {
      estr_uint_t _val = 0;
      switch (param_size(param)) {
#if PARAM_8BIT
        NUM_GET(uint8_t);
#endif /* PARAM_8BIT */
#if PARAM_16BIT
        NUM_GET(uint16_t);
#endif /* PARAM_16BIT */
#if PARAM_32BIT
        NUM_GET(uint32_t);
#endif /* PARAM_32BIT */
#if PARAM_64BIT
        NUM_GET(uint64_t);
#endif /* PARAM_64BIT */
      }
      return estr_putud(str, _val);
    }
#endif /* PARAM_UINT */
#undef NUM_GET
#define NUM_GET(type, exp) case sizeof(type): _val = PARAM_ROM_GET(*(const type*)val); _exp = exp; break
#if PARAM_REAL
    case param_real: {
      estr_real_t _val = 0.0;
      uint8_t _exp;
      switch (param_size(param)) {
#if PARAM_32BIT
        NUM_GET(float, 8);
#endif /* PARAM_32BIT */
#if PARAM_64BIT
        NUM_GET(double, 16);
#endif /* PARAM_64BIT */
      default:
        return param_error;
      }
      return estr_putrp(str, _val, _exp);
    }
#endif /* PARAM_REAL */
#if PARAM_CSTR
    case param_cstr:
      return estr_putsq_rom(str, val);
#endif /* PARAM_CSTR */
#if PARAM_HBIN
    case param_hbin:
      return estr_putsx(str, val, param_size(param));
#endif /* PARAM_HBIN */
#if PARAM_IPV4
    case param_ipv4: {
      const uint8_t *ptr = val;
      uint32_t i = 1, c = 0;
      
      c += estr_putud(str, PARAM_ROM_GET(*ptr++));
      for (; i < param_size(param); i++) {
        c += estr_putc(str, '.');
        c += estr_putud(str, PARAM_ROM_GET(*ptr++));
      }
      
      return c;
    }
#endif /* PARAM_IPV4 */
#if PARAM_MAC
    case param_mac: {
      const uint8_t *ptr = val;
      uint32_t i = 1, c = 0;

      c += estr_putux(str, PARAM_ROM_GET(*ptr++));
      for (; i < param_size(param); i++) {
        c += estr_putc(str, ':');
        c += estr_putux(str, PARAM_ROM_GET(*ptr++));
      }
      
      return c;
    }
#endif /* PARAM_MAC */
    default:
      break;
    }
  } else {
    return estr_putsc(str, "null");
  }
  
  return param_error;
}

#if PARAM_CONS
int param_getestr_cons(const param_desc_t *param, param_flag_t field, estr_t *str) {
  return getstr(param, param_cons(param, field), str);
}
#endif /* PARAM_CONS */

int param_getestr(const param_desc_t *param, estr_t *str) {
  uint8_t val[param_size(param)];
  int res = param_get(param, val);
  
  if (res != param_success) {
    return res;
  }
  
  return getstr(param, val, str);
}

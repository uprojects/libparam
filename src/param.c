#include "param.h"
#include "param-intern.h"

#if defined(PARAM_ROM_ALIGN) && PARAM_ROM_ALIGN > 1
#define PARAM_ROM_ALIGN_MASK (PARAM_ROM_ALIGN - 1)

typedef union {
#if PARAM_ROM_ALIGN == 8
  uint64_t v;
#elif PARAM_ROM_ALIGN == 4
  uint32_t v;
#elif PARAM_ROM_ALIGN == 2
  uint16_t v;
#endif /* PARAM_TOM_ALIGN */
  uint8_t p[PARAM_ROM_ALIGN];
} param_rom_cell_t;

void param_rom_read(void *ptr, const void *addr, size_t len) {
  uint8_t offset = (intptr_t)addr & PARAM_ROM_ALIGN_MASK;
  const param_rom_cell_t *a = (const void*)((intptr_t)addr & (intptr_t)~PARAM_ROM_ALIGN_MASK);
  uint8_t *d = ptr;

  if (offset > 0) {
    const param_rom_cell_t v = *a++;
    const uint8_t *p = &v.p[offset];
    for (; p <= &v.p[PARAM_ROM_ALIGN_MASK]; *d++ = *p++);
    len -= PARAM_ROM_ALIGN - offset;
  }

  const param_rom_cell_t *e = (const void*)(a + len / PARAM_ROM_ALIGN);
  len -= (e - a) * PARAM_ROM_ALIGN;
  for (; a < e; ) {
    const param_rom_cell_t v = *a++;
    const uint8_t *p = v.p;
    for (; p <= &v.p[PARAM_ROM_ALIGN_MASK]; *d++ = *p++);
  }
  
  if (len > 0) {
    const param_rom_cell_t v = *a;
    const uint8_t *p = &v.p[0];
    for (; p < &v.p[len]; *d++ = *p++);
  }
}

int param_rom_cmp(const void *ptr, const void *addr, size_t len) {
  uint8_t offset = (intptr_t)addr & PARAM_ROM_ALIGN_MASK;
  const param_rom_cell_t *a = (const void*)((intptr_t)addr & (intptr_t)~PARAM_ROM_ALIGN_MASK);
  const uint8_t *d = ptr;

  if (offset > 0) {
    const param_rom_cell_t v = *a++;
    const uint8_t *p = &v.p[offset];
    for (; p <= &v.p[PARAM_ROM_ALIGN_MASK]; )
      if (*d++ != *p++)
        return -1;
    len -= PARAM_ROM_ALIGN - offset;
  }

  const param_rom_cell_t *e = (const void*)(a + len / PARAM_ROM_ALIGN);
  len -= (e - a) * PARAM_ROM_ALIGN;
  for (; a < e; ) {
    const param_rom_cell_t v = *a++;
    const uint8_t *p = v.p;
    for (; p <= &v.p[PARAM_ROM_ALIGN_MASK]; )
      if (*d++ != *p++)
        return -1;
  }

  if (len > 0) {
    const param_rom_cell_t v = *a;
    const uint8_t *p = v.p;
    for (; p < &v.p[len]; )
      if (*d++ != *p++)
        return -1;
  }
  
  return 0;
}

param_size_t param_rom_len(const char *addr) {
  uint8_t offset = (intptr_t)addr & PARAM_ROM_ALIGN_MASK;
  const param_rom_cell_t *a = (const void*)((intptr_t)addr & (intptr_t)~PARAM_ROM_ALIGN_MASK);
  param_size_t len = 0;
  
  if (offset > 0) {
    const param_rom_cell_t v = *a++;
    const uint8_t *p = &v.p[offset];
    for (; p <= &v.p[PARAM_ROM_ALIGN_MASK]; len++)
      if (*p++ == '\0')
        return len;
  }

  for (; ; ) {
    const param_rom_cell_t v = *a++;
    const uint8_t *p = v.p;
    for (; p <= &v.p[PARAM_ROM_ALIGN_MASK]; len++)
      if (*p++ == '\0')
        return len;
  }
  
  return -1;
}
#endif /* defined(PARAM_ROM_ALIGN) && PARAM_ROM_ALIGN > 1 */

#ifdef __GNUC__

#define fields_count(flag) __builtin_popcount(flag)
#define bit_index(subj) __builtin_ctz(subj)

#else /* __GNUC__ */

static const int8_t _fields_count[16] = {
  /*[0b0000]=*/0,
  /*[0b0001]=*/1,
  /*[0b0010]=*/1,
  /*[0b0011]=*/2,
  /*[0b0100]=*/1,
  /*[0b0101]=*/2,
  /*[0b0110]=*/2,
  /*[0b0111]=*/3,
  /*[0b1000]=*/1,
  /*[0b1001]=*/2,
  /*[0b1010]=*/2,
  /*[0b1011]=*/3,
  /*[0b1100]=*/2,
  /*[0b1101]=*/3,
  /*[0b1110]=*/3,
  /*[0b1111]=*/4,
};

#define fields_count(flag) _fields_count[flag]

static const int8_t _bit_index[9] = {
  /*[0b0000]=*/-1,
  /*[0b0001]=*/0,
  /*[0b0010]=*/1,
  /*[0b0011]=*/-1,
  /*[0b0100]=*/2,
  /*[0b0101]=*/-1,
  /*[0b0110]=*/-1,
  /*[0b0111]=*/-1,
  /*[0b1000]=*/3,
};

#define bit_index(subj) _bit_index[subj]

#endif /* __GNUC__ */

static const int8_t _field_index[16][4] = {
  /*[0b0000]=*/{-1, -1, -1, -1},
  /*[0b0001]=*/{ 0, -1, -1, -1},
  /*[0b0010]=*/{-1,  0, -1, -1},
  /*[0b0011]=*/{ 0,  1, -1, -1},
  /*[0b0100]=*/{-1, -1,  0, -1},
  /*[0b0101]=*/{ 0, -1,  1, -1},
  /*[0b0110]=*/{-1,  0,  1, -1},
  /*[0b0111]=*/{ 0,  1,  2, -1},
  /*[0b1000]=*/{-1, -1, -1,  0},
  /*[0b1001]=*/{ 0, -1, -1,  1},
  /*[0b1010]=*/{-1,  0, -1,  1},
  /*[0b1011]=*/{ 0,  1, -1,  2},
  /*[0b1100]=*/{-1, -1,  0,  1},
  /*[0b1101]=*/{ 0, -1,  1,  2},
  /*[0b1110]=*/{-1,  0,  1,  2},
  /*[0b1111]=*/{ 0,  1,  2,  3},
};

#define field_index(flag, subj) (_field_index[flag][bit_index(subj)])

#if PARAM_CONS
const param_cell_t *param_cons(const param_desc_t *param, param_flag_t field) {
  param_flag_t count = field & ~param_cons_mask;
  field &= param_cons_mask;
  
  param_flag_t flags = param_flag(param);
  const uint8_t *cons = PARAM_ROM_GET(param->cons);
  
  if (cons == NULL || !(field == param_none || (flags & field))
#if PARAM_COMP
      /* Non-atomic parameters has no min/max/step constrains */
      || (flags & param_comp_mask
#if PARAM_DEF
          && field != param_def
#endif /* PARAM_DEF */
          )
#endif /* PARAM_COMP */
      ) return NULL;

  flags &= param_cons_mask;
  flags >>= param_cons_left;

  return cons + param_size(param) *
    (field == param_none ?
     fields_count(flags) + count :
     field_index(flags, field >> param_cons_left));
}
#endif /* PARAM_CONS */

#if PARAM_TEXT
const char *param_text(const param_desc_t *param, param_flag_t field) {
  param_flag_t count = field & ~param_text_mask;
  field &= param_text_mask;
  
  param_flag_t flags = param_flag(param) & param_text_mask;
  const char *const *text = PARAM_ROM_GET(param->text);
  
  if (text == NULL || !(field == param_none || (flags & field)))
    return NULL;
  
  flags >>= param_text_left;

  return PARAM_ROM_GET(text[field == param_none ?
                            fields_count(flags) + count :
                            field_index(flags, field >> param_text_left)]);
}
#endif /* PARAM_TEXT */

#if PARAM_ENUM
param_size_t param_enum_count(const param_desc_t *param) {
  const param_cell_t *count = param_cons(param, param_none);

  if (count != NULL) {
    switch (param_size(param)) {
#define ENUM_LEN(type) case sizeof(type): return PARAM_ROM_GET(*(type*)count);
#if PARAM_8BIT
      ENUM_LEN(uint8_t);
#endif
#if PARAM_16BIT
      ENUM_LEN(uint16_t);
#endif
#if PARAM_32BIT
      ENUM_LEN(uint32_t);
#endif
#if PARAM_64BIT
      ENUM_LEN(uint64_t);
#endif
#undef ENUM_LEN
    }
  }
  
  return 0;
}

#if PARAM_TEXT
const char *param_enum_text(const param_desc_t *param, param_flag_t field, param_size_t index) {
  const param_flag_t flags = (param_flag(param) & param_text_mask) >> param_text_left;
  
  return param_text(param, index * (
#if PARAM_NAME
                                    1 +
#endif /* PARAM_NAME */
                                    fields_count(flags))
#if PARAM_NAME
                    + (field == param_none ? 0 :
                       (1
#endif /* PARAM_NAME */
                        + field_index(flags, field >> param_text_left)
#if PARAM_NAME
                        ))
#endif /* PARAM_NAME */
                       );
}
#endif /* PARAM_TEXT */
#endif /* PARAM_ENUM */

#if PARAM_UFIX || PARAM_SFIX
param_size_t param_fix_fract(const param_desc_t *param) {
  const param_cell_t *count = param_cons(param, param_none);

  if (count != NULL) {
    switch (param_size(param)) {
#define FIX_FRACT(type) case sizeof(type): return PARAM_ROM_GET(*(type*)count);
#if PARAM_8BIT
      FIX_FRACT(uint8_t);
#endif
#if PARAM_16BIT
      FIX_FRACT(uint16_t);
#endif
#if PARAM_32BIT
      FIX_FRACT(uint32_t);
#endif
#if PARAM_64BIT
      FIX_FRACT(uint64_t);
#endif
#undef FIX_FRACT
    }
  }
  
  return 0;
}
#endif

static int basic_get(const param_desc_t *param, param_cell_t *value) {
  const void *cell = param_cell(param);
  
  if (cell == NULL)
    return param_error;
  
  memcpy(value, cell,
#if PARAM_CSTR
         param_type(param) == param_cstr ? strlen(cell) + 1 /*'\0'*/ :
#endif /* PARAM_CSTR */
         param_size(param));
  return param_success;
}

static int basic_set(const param_desc_t *param, const param_cell_t *value) {
  void *cell = param_cell(param);
  
  if (cell == NULL)
    return param_error;
  
  PARAM_ROM_READ(cell, value,
#if PARAM_CSTR
         param_type(param) == param_cstr ? strlen(value) + 1 /*'\0'*/ :
#endif /* PARAM_CSTR */
         param_size(param));
  return param_success;
}

int param_raw_get(const param_desc_t *param, param_cell_t *val) {
  param_get_t *get =
#if PARAM_VIRT
    param_flag(param) & param_virtual ? PARAM_ROM_GET(((const param_vmt_t*)param_cell(param))->get) :
#endif /* PARAM_VIRT */
    basic_get;
  
  if (get == NULL) {
    return param_error;
  }

  int res = get(param, val);

  if (res != param_success) {
    return res;
  }

#if PARAM_FIX && PARAM_ATOM
  if (param_atomic(param)) {
#if PARAM_MIN
#define CHK_MIN(ctype)                                    \
    const ctype* min = param_cons(param, param_min);      \
    if (min != NULL && *pval < PARAM_ROM_GET(*min)) return param_error
#else /* PARAM_MIN */
#define CHK_MIN(ctype)
#endif /* PARAM_MIN */
#if PARAM_MAX
#define CHK_MAX(ctype)                                    \
    const ctype* max = param_cons(param, param_max);      \
    if (max != NULL && *pval > PARAM_ROM_GET(*max)) return param_error
#else /* PARAM_MAX */
#define CHK_MAX(ctype)
#endif /* PARAM_MAX */
#if PARAM_MIN && PARAM_MAX
#define CHK_NUM(ctype)            \
    case sizeof(ctype): {         \
      ctype *pval = (ctype*) val; \
      CHK_MIN(ctype);             \
      CHK_MAX(ctype);             \
    } break
#else /* PARAM_MIN && PARAM_MAX */
#define CHK_NUM(ctype)
#endif /* PARAM_MIN && PARAM_MAX */
    
    switch (param_type(param)) {
#if PARAM_SINT || PARAM_SFIX
#if PARAM_SINT
    case param_sint:
#endif /* PARAM_SINT */
#if PARAM_SFIX
    case param_sfix:
#endif /* PARAM_SFIX */
      switch (param_size(param)) {
#if PARAM_8BIT
        CHK_NUM(int8_t);
#endif
#if PARAM_16BIT
        CHK_NUM(int16_t);
#endif
#if PARAM_32BIT
        CHK_NUM(int32_t);
#endif
#if PARAM_64BIT
        CHK_NUM(int64_t);
#endif
      }
      break;
#endif /* PARAM_SINT || PARAM_SFIX */
#if PARAM_UINT || PARAM_UFIX || PARAM_ENUM
#if PARAM_UINT
    case param_uint:
#endif /* PARAM_UINT */
#if PARAM_UFIX
    case param_ufix:
#endif /* PARAM_UFIX */
#if PARAM_ENUM
    case param_enum:
#endif /* PARAM_ENUM */
      switch (param_size(param)) {
#if PARAM_8BIT
        CHK_NUM(uint8_t);
#endif
#if PARAM_16BIT
        CHK_NUM(uint16_t);
#endif
#if PARAM_32BIT
        CHK_NUM(uint32_t);
#endif
#if PARAM_64BIT
        CHK_NUM(uint64_t);
#endif
      }
      break;
#endif /* PARAM_UINT || PARAM_UFIX || PARAM_ENUM */
#if PARAM_REAL
    case param_real:
      switch (param_size(param)) {
#if PARAM_32BIT
        CHK_NUM(float);
#endif
#if PARAM_64BIT
        CHK_NUM(double);
#endif
      }
      break;
#endif /* PARAM_REAL */
    default:
      break;
    }
  }
#endif /* PARAM_FIX */
  
  return param_success;
}

int param_get(const param_desc_t *param, param_cell_t *val) {
  return param_raw_get(param, val);
}

int param_raw_set(const param_desc_t *param, const param_cell_t *val) {
  param_set_t *set =
#if PARAM_VIRT
    param_flag(param) & param_virtual ? PARAM_ROM_GET(((const param_vmt_t*)param_cell(param))->set) :
#endif /* PARAM_VIRT */
    basic_set;

  if (set == NULL)
    return param_error;

  if (val != NULL) {
#if PARAM_FIX
    if (param_atomic(param)) {
#if PARAM_ATOM
      uint8_t _val[param_size(param)];
      
#if PARAM_MIN
#define FIX_MIN(ctype)                                 \
      const ctype* min = param_cons(param, param_min); \
      if (min != NULL && *pval < PARAM_ROM_GET(*min))  \
        *pval = PARAM_ROM_GET(*min)
#else /* PARAM_MIN */
#define FIX_MIN(ctype)
#endif /* PARAM_MIN */
#if PARAM_MAX
#define FIX_MAX(ctype)                                 \
      const ctype* max = param_cons(param, param_max); \
      if (max != NULL && *pval > PARAM_ROM_GET(*max))  \
        *pval = PARAM_ROM_GET(*max)
#else /* PARAM_MAX */
#define FIX_MAX(ctype)
#endif /* PARAM_MAX */
#if PARAM_STP
#define FIX_STP(ctype, rnd)                               \
      const ctype* stp = param_cons(param, param_stp);    \
      if (stp != NULL && min != NULL)                     \
        *pval = PARAM_ROM_GET(*stp) *                     \
          rnd((*pval - PARAM_ROM_GET(*min)) /             \
              PARAM_ROM_GET(*stp)) + PARAM_ROM_GET(*min); \
      if (stp != NULL && max != NULL)                     \
        *pval = PARAM_ROM_GET(*max) - PARAM_ROM_GET(*stp) \
          * rnd((PARAM_ROM_GET(*max) - *pval) /           \
                PARAM_ROM_GET(*stp))
#else /* PARAM_STP */
#define FIX_STP(ctype, rnd)
#endif /* PARAM_STP */
#define FIX_NUM(ctype)                   \
      case sizeof(ctype): {              \
        ctype *pval = (ctype*) _val;     \
        *pval = *((const ctype*)val);    \
        val = (const param_cell_t*)pval; \
        FIX_MIN(ctype);                  \
        FIX_MAX(ctype);                  \
        FIX_STP(ctype, (uint32_t));      \
      } break
      
      switch (param_type(param)) {
#if PARAM_SINT || PARAM_SFIX
#if PARAM_SINT
      case param_sint:
#endif /* PARAM_SINT */
#if PARAM_SFIX
      case param_sfix:
#endif /* PARAM_SFIX */
        switch (param_size(param)) {
#if PARAM_8BIT
          FIX_NUM(int8_t);
#endif
#if PARAM_16BIT
          FIX_NUM(int16_t);
#endif
#if PARAM_32BIT
          FIX_NUM(int32_t);
#endif
#if PARAM_64BIT
          FIX_NUM(int64_t);
#endif
        }
        break;
#endif /* PARAM_SINT || PARAM_SFIX */
#if PARAM_UINT || PARAM_UFIX || PARAM_ENUM
#if PARAM_UINT
      case param_uint:
#endif /* PARAM_UINT */
#if PARAM_UFIX
      case param_ufix:
#endif /* PARAM_UFIX */
#if PARAM_ENUM
      case param_enum:
#endif /* PARAM_ENUM */
        switch (param_size(param)) {
#if PARAM_8BIT
          FIX_NUM(uint8_t);
#endif
#if PARAM_16BIT
          FIX_NUM(uint16_t);
#endif
#if PARAM_32BIT
          FIX_NUM(uint32_t);
#endif
#if PARAM_64BIT
          FIX_NUM(uint64_t);
#endif
        }
        break;
#endif /* PARAM_UINT || PARAM_UFIX || PARAM_ENUM */
#if PARAM_REAL
      case param_real:
        switch (param_size(param)) {
#if PARAM_32BIT
          FIX_NUM(float);
#endif
#if PARAM_64BIT
          FIX_NUM(double);
#endif
        }
        break;
#endif /* PARAM_REAL */
      default:
        break;
      }

      return set(param, val);
#endif /* PARAM_ATOM */
    } else {
      switch (param_type(param)) {
#if PARAM_CSTR
      case param_cstr:
        if (strlen(val) + 1 > param_size(param)) {
          return param_error;
        }
#endif /* PARAM_CSTR */
      default:
        break;
      }
    }
#endif /* PARAM_FIX */
  } else { /* val == NULL */
#if PARAM_DEF
#define SET_DEF val = param_cons(param, param_def);
#else /* PARAM_DEF */
#define SET_DEF
#endif /* PARAM_DEF */
    SET_DEF;
  }
  
  return val ? set(param, val) : param_error;
}

int param_set(const param_desc_t *param, const param_cell_t *val) {
  int status = param_raw_set(param, val);

#if PARAM_PERS
  if (status == param_success) {
    param_save(param);
  }
#endif /* PARAM_PERS */
  
  return status;
}

#if PARAM_NAME || PARAM_TEXT
const param_desc_t *param_find_text(const param_coll_t *params, param_flag_t field, const char *value, param_size_t length) {
  const param_desc_t *param = PARAM_ROM_GET(params->param), *param_ = param + PARAM_ROM_GET(params->count);
  
  for (; param != param_; param ++) {
    const char *text = field == param_none ?
#if PARAM_NAME
      param_name(param)
#else /* PARAM_NAME */
      NULL
#endif /* PARAM_NAME */
      :
#if PARAM_TEXT
      param_text(param, field)
#else /* PARAM_TEXT */
      NULL
#endif /* PARAM_TEXT */
      ;
    if (text != NULL && length == PARAM_ROM_LEN(text) && 0 == PARAM_ROM_CMP(value, text, length)) {
      return param;
    }
  }
  
  return NULL;
}
#endif /* PARAM_NAME || PARAM_TEXT */

void param_init(const param_coll_t *params) {
  const param_desc_t *param = PARAM_ROM_GET(params->param), *param_ = param + PARAM_ROM_GET(params->count);
  
  for (; param != param_; param ++) {
#if PARAM_DEF
    param_raw_set(param, NULL); /* set default value */
#endif /* PARAM_DEF */
  }

#if PARAM_PERS
  param_load_all(params);
#endif /* PARAM_PERS */
}

#if PARAM_NAME

#define _param_djb2_init 5381
#if PARAM_FMUL
#define _param_djb2_calc(h, c) (h * 33 + c)
#else /* PARAM_FMUL */
#define _param_djb2_calc(h, c) ((h << 5) + h + c)
#endif /* PARAM_FMUL */

#define _param_sdbm_init 0
#if PARAM_FMUL
#define _param_sdbm_calc(h, c) (h * 65599 + c)
#else /* PARAM_FMUL */
#define _param_sdbm_calc(h, c) ((h << 6) + (h << 16) - h + c)
#endif /* PARAM_FMUL */

#define _param_hash_(ht, op) _param_##ht##_##op
#define _param_hash(ht, op) _param_hash_(ht, op)

param_uuid_t param_uuid(const param_desc_t *param) {
  const char *name = param_name(param);
  param_uuid_t uuid = _param_hash(PARAM_HASH, init);
  
  for (; *name != '\0'; )
    uuid = _param_hash(PARAM_HASH, calc)(uuid, *name++);
  
  return uuid;
}
#endif /* PARAM_NAME */

#if PARAM_PERS
int param_load_all(const param_coll_t *params) {
  const param_desc_t *param = PARAM_ROM_GET(params->param), *param_ = param + PARAM_ROM_GET(params->count);
  
  for (; param != param_; param ++) param_load(param);
  
  return param_success;
}

int param_save_all(const param_coll_t *params) {
  const param_desc_t *param = PARAM_ROM_GET(params->param), *param_ = param + PARAM_ROM_GET(params->count);
  
  for (; param != param_; param ++) param_save(param);
  
  return param_success;
}
#endif /* PARAM_PERS */

#include "param.h"
#include "param-sio.h"
#include "param-json.h"
#include "json_parser.h"

#if PARAM_NAME

#ifndef PARAM_JSON_LIMIT
#define PARAM_JSON_LIMIT 64
#endif /* PARAM_JSON_LIMIT */

static const char error_overflow[] = "{\"error\":\"Too many parameters\"}";
static const char error_invalid[] = "{\"error\":\"Invalid json request\"}";

#define json_put_str put_quoted_str

static void json_put_null(char **ptr, char *end){
  if (end - *ptr >= 4) {
    *(*ptr)++ = 'n';
    *(*ptr)++ = 'u';
    *(*ptr)++ = 'l';
    *(*ptr)++ = 'l';
  }
}

static void json_put_bool(char val, char **ptr, char *end){
  if (val) {
    if (end - *ptr >= 4) {
      *(*ptr)++ = 't';
      *(*ptr)++ = 'r';
      *(*ptr)++ = 'u';
      *(*ptr)++ = 'e';
    }
  } else {
    if (end - *ptr >= 5) {
      *(*ptr)++ = 'f';
      *(*ptr)++ = 'a';
      *(*ptr)++ = 'l';
      *(*ptr)++ = 's';
      *(*ptr)++ = 'e';
    }
  }
}

static void json_put_sep(char **ptr, char *end){
  if (end - *ptr >= 1 && *(*ptr - 1) != '{' && *(*ptr - 1) != '[') {
    *(*ptr)++ = ',';
  }
}

static void json_put_col(char **ptr, char *end){
  if (end - *ptr >= 1) {
    *(*ptr)++ = ':';
  }
}

static void json_put_flag_field(const param_desc_t *param, param_flag_t field, const char *name, char **ptr, char *end) {
  if (param->flag & field) {
    json_put_sep(ptr, end);
    json_put_str(ptr, end, name);
  }
}

#if PARAM_CONS && (PARAM_DEF || PARAM_MIN || PARAM_MAX || PARAM_STP)
static void json_put_cons_field(const param_desc_t *param, param_flag_t field, const char *name, char **ptr, char *end) {
  const param_cell_t *cons = param_cons(param, field);
  if (cons == NULL) return;
  char *cp = *ptr;
  json_put_sep(ptr, end);
  json_put_str(ptr, end, name);
  json_put_col(ptr, end);
  if (param_success != param_getstr_cons(param, field, ptr, end)) {
    *ptr = cp;
  }
}
#endif /* PARAM_CONS && (PARAM_DEF || PARAM_MIN || PARAM_MAX || PARAM_STP) */

#if PARAM_TEXT
static void json_put_text_field_ptr(const char *text, const char *name, char **ptr, char *end) {
  if (text == NULL) return;
  json_put_sep(ptr, end);
  json_put_str(ptr, end, name);
  json_put_col(ptr, end);
  json_put_str(ptr, end, text);
}

static void json_put_text_field(const param_desc_t *param, param_flag_t field, const char *name, char **ptr, char *end) {
  const char *text = param_text(param, field);
  json_put_text_field_ptr(text, name, ptr, end);
}
#endif /* PARAM_TEXT */

#if PARAM_ENUM && PARAM_TEXT
static void json_put_enum_field(const param_desc_t *param, char **ptr, char *end) {
  param_size_t count = param_enum_count(param);
  if (count > 0) {
    json_put_sep(ptr, end);
    json_put_str(ptr, end, "enum");
    json_put_col(ptr, end);
    *(*ptr)++ = '{';
    param_size_t index = 0;
    for (; index < count; index++) {
      const char *name = param_enum_text(param, param_none, index);
      if (name == NULL) continue;
      json_put_sep(ptr, end);
      json_put_str(ptr, end, name);
      json_put_col(ptr, end);
#if PARAM_INFO
      const char *text = param_enum_text(param, param_info, index);
      if (text == NULL) {
#endif /* PARAM_INFO */
        json_put_null(ptr, end);
#if PARAM_INFO
      } else {
        json_put_str(ptr, end, text);
      }
#endif /* PARAM_INFO */
    }
    *(*ptr)++ = '}';
  }
}
#else /* PARAM_ENUM && PARAM_TEXT */
#define json_put_enum_field(param, ptr, end)
#endif /* PARAM_ENUM && PARAM_TEXT */

typedef enum {
  request_init,
  request_type,
  request_data,
  request_key,
  request_val,
  request_done,
  request_end,
} request_stage_t;

typedef enum {
  request_info,
  request_gets,
  request_sets,
} request_type_t;

typedef struct {
  request_type_t type;
  union {
    struct {
      param_num_t param;
    } info;
    struct {
      param_num_t count;
      param_num_t param[PARAM_JSON_LIMIT];
    } gets;
    struct {
      param_num_t count;
      struct {
        param_num_t num;
        uint8_t len;
        uint16_t off;
      } param[PARAM_JSON_LIMIT];
    } sets;
  } data;
} request_t;

typedef struct {
  json_tokenizer tokenizer;
  const param_coll_t *params;
  const char *source;
  request_stage_t stage;
  request_t *request;
} request_parser_t;

static int request_callback(json_tokenizer *tok, json_tokenizer_flags flags, const char *ptr, size_t len) {
  request_parser_t *parser = (request_parser_t*)tok;
  request_t *req = parser->request;

#define goto_next return 1
#define goto_fail return 0
  
  switch (parser->stage) {
  case request_init: /* { */
    switch (flags) {
    case json_object_open:
      parser->stage = request_type;
      goto_next;
    default:
      goto_fail;
    }
  case request_type: /* {"req_type": */
    switch (flags) {
    case json_string_chunk: /* {"req_type" */
      if (len < 1) { /* {"" */
        goto_fail;
      }
      switch (*ptr) {
      case 'i': /* {"i..." */
        req->type = request_info;
        goto_next;
      case 'g': /* {"g..." */
        req->type = request_gets;
        req->data.gets.count = 0;
        goto_next;
      case 's': /* {"s..." */
        req->type = request_sets;
        req->data.sets.count = 0;
        goto_next;
      default:
        goto_fail;
      }
    case json_kv_separate: /* {"req_type": */
      parser->stage = request_data;
      goto_next;
    default:
      goto_fail;
    }
  case request_data: /* {"req_type":req_data */
    switch (req->type) {
    case request_info: /* {"info":info_data */
      switch (flags) {
      case json_null_chunk: /* {"info":null */
        req->data.info.param = param_num_none;
        parser->stage = request_done;
        goto_next;
      case json_string_chunk: /* {"info":"param_name" */
        {
          const param_desc_t *param = param_find(parser->params, ptr, len);
          req->data.info.param = param ? param_num(parser->params, param) : param_num_none;
        }
        parser->stage = request_done;
        goto_next;
      default:
        goto_fail;
      }
    case request_gets: /* {"g...":[ */
      switch (flags) {
      case json_array_open:
        parser->stage = request_val;
        goto_next;
      default:
        goto_fail;
      }
    case request_sets: /* {"s...":{ */
      switch (flags) {
      case json_object_open:
        parser->stage = request_key;
        goto_next;
      default:
        goto_fail;
      }
    }
  case request_key:
    switch (req->type) {
    case request_sets: /* {"s...":{... */
      switch (flags) {
      case json_string_chunk: /* {"s...":{"param_name" */
        {
          if (req->data.gets.count >= PARAM_JSON_LIMIT) {
            goto_fail;
          }
          const param_desc_t *param = param_find(parser->params, ptr, len);
          req->data.sets.param[req->data.sets.count].num =
            param ? param_num(parser->params, param) : param_num_none;
        }
        goto_next;
      case json_kv_separate: /* {"s...":{"param_name": */
        parser->stage = request_val;
        goto_next;
      case json_object_close: /* {"s...":{...} */
        parser->stage = request_done;
        goto_next;
      default:
        goto_fail;
      }
    default:
      goto_fail;
    }
  case request_val:
    switch (req->type) {
    case request_gets:
      switch (flags) {
      case json_string_chunk: /* {"g...":["param_name" */
        {
          if (req->data.gets.count >= PARAM_JSON_LIMIT) {
            goto_fail;
          }
          const param_desc_t *param = param_find(parser->params, ptr, len);
          req->data.gets.param[req->data.gets.count ++] =
            param ? param_num(parser->params, param) : param_num_none;
        }
      case json_el_separate: /* {"g...":["param_name", */
        goto_next;
      case json_array_close: /* {"g...":["param_name"] */
        parser->stage = request_done;
        goto_next;
      default:
        goto_fail;
      }
    case request_sets:
      switch (flags) {
      case json_null_chunk: /* {"s...":{"param_name":null_value */
        req->data.sets.param[req->data.sets.count ++].len = 0;
        goto_next;
      case json_bool_chunk: /* {"s...":{"param_name":bool_value */
      case json_number_chunk: /* {"s...":{"param_name":number_value */
      case json_string_chunk: /* {"s...":{"param_name":"string_value" */
        req->data.sets.param[req->data.sets.count].off = ptr - parser->source;
        req->data.sets.param[req->data.sets.count ++].len = len;
        goto_next;
      case json_el_separate:
        parser->stage = request_key; /* {"s...":{"param_name":param_value, */
        goto_next;
      case json_object_close: /* {"s...":{"param_name":param_value} */
        parser->stage = request_done;
        goto_next;
      default:
        goto_fail;
      }
    default:
      goto_fail;
    }
  case request_done: /* {"req_type":req_data} */
    switch (flags) {
    case json_object_close: /* {"req_type":...} */
      parser->stage = request_end;
      goto_next;
    default:
      goto_fail;
    }
  default:
    goto_fail;
  }
}

int param_json_handle(const param_coll_t *params,
                      const char *iptr, const char *iend,
                      char *optr, char *oend) {
  request_t request, *req = &request;
  
  {
    request_parser_t parser;
    
    parser.params = params;
    parser.source = iptr;
    parser.stage = request_init;
    parser.request = req;
    
    json_tokenizer_init(&parser.tokenizer);
    int res = json_tokenizer_execute(&parser.tokenizer, request_callback, iptr, iend - iptr);
    
    if (json_tokenizer_failed(res)) {
      if ((req->type == request_gets &&
           req->data.gets.count == param_num_none) ||
          (req->type == request_sets &&
           req->data.sets.count == param_num_none)) {
        memcpy(optr, error_overflow, sizeof(error_overflow)-1);
        return sizeof(error_overflow)-1;
      }
      
      memcpy(optr, error_invalid, sizeof(error_invalid)-1);
      return sizeof(error_invalid)-1;
    }
    
    if (parser.stage != request_end) {
      return 0;
    }
  }
  
  const char *ptr = optr;
  
  switch (req->type) {
  case request_info:
    if (req->data.info.param == param_num_none) {
      const param_desc_t *param = PARAM_ROM_GET(params->param);
      const param_desc_t *param_end = param + PARAM_ROM_GET(params->count);
      *optr++ = '[';
      for (; param < param_end && optr < oend; param ++) {
        const char *name = param_name(param);
        if (name) {
          json_put_sep(&optr, oend);
          json_put_str(&optr, oend, name);
        }
      }
      *optr++ = ']';
    } else {
      const param_desc_t *param = param_desc(params, req->data.info.param);
      char *cp; /* checkpoint */
      param_flag_t type = param_type(param);
      *optr++ = '{';
      if (type != param_none) {
        cp = optr;
        json_put_sep(&optr, oend);
        json_put_str(&optr, oend, "type");
        json_put_col(&optr, oend);
        *optr++ = '"';
        if (param_success == param_getstr_type(param, &optr, oend)) {
          *optr++ = '"';
        } else {
          optr = cp;
        }
      }
      
      if (param_size(param) != 0) {
        cp = optr;
        json_put_sep(&optr, oend);
        json_put_str(&optr, oend, "size");
        json_put_col(&optr, oend);
        if (param_success != param_getstr_size(param, &optr, oend)) {
          optr = cp;
        }
      }
      
      if ((param_flag(param) & (param_getable | param_setable
#if PARAM_PERS
                                | param_persist
#endif /* PARAM_PERS */
#if PARAM_VIRT && 0
                                | param_virtual
#endif /* PARAM_VIRT */
                                )) != param_none) {
        json_put_sep(&optr, oend);
        json_put_str(&optr, oend, "flag");
        json_put_col(&optr, oend);
        *optr++ = '[';
        json_put_flag_field(param, param_getable, "getable", &optr, oend);
        json_put_flag_field(param, param_setable, "setable", &optr, oend);
#if PARAM_PERS
        json_put_flag_field(param, param_persist, "persist", &optr, oend);
#endif /* PARAM_PERS */
#if PARAM_VIRT && 0
        json_put_flag_field(param, param_virtual, "virtual", &optr, oend);
#endif /* PARAM_VIRT */
        *optr++ = ']';
      }
      
#if PARAM_DEF
      json_put_cons_field(param, param_def, "def", &optr, oend);
#endif /* PARAM_DEF */
#if PARAM_ENUM
      if (param_type(param) != param_enum) {
#endif /* PARAM_ENUM */
#if PARAM_MIN
        json_put_cons_field(param, param_min, "min", &optr, oend);
#endif /* PARAM_MIN */
#if PARAM_MAX
        json_put_cons_field(param, param_max, "max", &optr, oend);
#endif /* PARAM_MAX */
#if PARAM_STP
        json_put_cons_field(param, param_stp, "stp", &optr, oend);
#endif /* PARAM_STP */
#if PARAM_ENUM
      }
#endif /* PARAM_ENUM */
      
#if PARAM_UNIT
      json_put_text_field(param, param_unit, "unit", &optr, oend);
#endif /* PARAM_UNIT */
#if PARAM_INFO
      json_put_text_field(param, param_info, "info", &optr, oend);
#endif /* PARAM_INFO */
#if PARAM_ITEM
      json_put_text_field(param, param_item, "item", &optr, oend);
#endif /* PARAM_ITEM */
#if PARAM_HINT
      json_put_text_field(param, param_hint, "hint", &optr, oend);
#endif /* PARAM_HINT */
#if PARAM_ENUM
      if (param_type(param) == param_enum) {
        json_put_enum_field(param, &optr, oend);
      }
#endif /* PARAM_ENUM */
      *optr++ = '}';
    }
    break;
  case request_gets:
    {
      *optr++ = '[';
      param_num_t i = 0;
      for (; i < req->data.gets.count; i ++) {
        json_put_sep(&optr, oend);
        param_num_t num = req->data.gets.param[i];
        if (num == param_num_none) {
          json_put_null(&optr, oend);
          continue;
        }
        const param_desc_t *param = param_desc(params, num);
        if (!(param_flag(param) & param_getable) ||
            param_success != param_getstr(param, &optr, oend)) {
          json_put_null(&optr, oend);
        }
      }
      *optr++ = ']';
    }
    break;
  case request_sets:
    {
      int8_t status[req->data.sets.count];
      param_num_t i = 0;
      for (; i < req->data.sets.count; i ++) {
        param_num_t num = req->data.sets.param[i].num;
        if (num == param_num_none) {
          continue;
        }
        const param_desc_t *param = param_desc(params, num);
        const char *vptr = iptr + req->data.sets.param[i].off;
        status[i] = param_flag(param) & param_setable ?
          ((req->data.sets.param[i].len > 0 ?
            param_success != param_setstr(param, &vptr, iptr + req->data.sets.param[i].off + req->data.sets.param[i].len) :
            param_success != param_set(param, NULL)) ? 0 : 1) : -1;
      }

      *optr++ = '{';
      for (i = 0; i < req->data.sets.count; i ++) {
        param_num_t num = req->data.sets.param[i].num;
        if (num == param_num_none) {
          continue;
        }
        const param_desc_t *param = param_desc(params, num);
        json_put_sep(&optr, oend);
        json_put_str(&optr, oend, param_name(param));
        json_put_col(&optr, oend);
        switch (status[i]) {
        case -1:
          json_put_null(&optr, oend);
          break;
        case 0:
          json_put_bool(0, &optr, oend);
          break;
        case 1:
          json_put_bool(1, &optr, oend);
          break;
        }
      }
      *optr++ = '}';
    }
    break;
  }
  
  return optr - ptr;
}

#endif /* PARAM_NAME */

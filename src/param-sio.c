#include "param.h"
#include "param-sio.h"

#if PARAM_TYPE
#if PARAM_64BIT
typedef int64_t  sint_t;
typedef uint64_t uint_t;
typedef double   real_t;
#elif PARAM_32BIT
typedef int32_t  sint_t;
typedef uint32_t uint_t;
typedef float    real_t;
#elif PARAM_16BIT
typedef int16_t  sint_t;
typedef uint16_t uint_t;
#elif PARAM_8BIT
typedef int8_t  sint_t;
typedef uint8_t uint_t;
#endif

#define min_uint ((uint_t)0)
#define max_uint (~min_uint)

#define min_sint ((sint_t)(max_uint>>1))
#define max_sint (-min_sint-1)

#if PARAM_UINT || PARAM_SINT || PARAM_UFIX || PARAM_SFIX || PARAM_REAL || PARAM_IPV4
static int stou_max_(const char **ptr, const char *end, uint_t *val, uint_t max, uint_t maxd10){
  for (*val = 0; *ptr < end; (*ptr)++) {
    if (**ptr >= '0' && **ptr <= '9') {
      uint8_t dig = **ptr - '0';
      if (*val > maxd10) {
        return param_overflow;
      }
      *val *= 10;
      if (max - *val < dig) {
        return param_overflow;
      }
      *val += dig;
    } else {
      return param_invalid;
    }
  }
  return param_success;
}

#define stou_max(ptr, end, val, max) stou_max_(ptr, end, val, (max), (max)/10)
#endif /* PARAM_UINT || PARAM_SINT || PARAM_UFIX || PARAM_SFIX || PARAM_IPV4 || PARAM_REAL */

#if PARAM_UINT || PARAM_UFIX || PARAM_SFIX || PARAM_REAL
static int stou(const char **ptr, const char *end, uint_t *val){
  return stou_max(ptr, end, val, max_uint);
}
#endif /* PARAM_UINT || PARAM_UFIX || PARAM_SFIX || PARAM_REAL */

#if PARAM_SINT
static int stoi(const char **ptr, const char *end, sint_t *val){
  char neg = 0;
  
  if (*ptr == end) {
    return param_underflow;
  }
  
  if (**ptr == '-') { /* skip sign */
    neg = 1;
    (*ptr)++;
  }
  
  int res = neg ? stou_max(ptr, end, (uint_t*)val, -min_sint) : stou_max(ptr, end, (uint_t*)val, max_sint);

  if (res != param_success) {
    return res;
  }
  
  if (neg) {
    *val = -*val;
  }
  
  return param_success;
}
#endif /* PARAM_SINT */

#if PARAM_SFIX || PARAM_UFIX
static int stouf(const char **ptr, const char *end, uint_t *val, uint8_t exp){
  uint_t decim_;
  
  const char *pch = *ptr;
  int res = stou(ptr, end, &decim_);
  
  if (*ptr > pch || **ptr == '.') {
    *val = decim_ << exp;
  }
  
  if (*ptr < end && **ptr == '.') {
    (*ptr)++;
    
    pch = *ptr; /* save point character */
    
    uint_t fract_;
    res = stou(ptr, end, &fract_);
    
    if (*ptr == pch) {
      goto final;
    }
    
    uint_t divis_ = 1;
    for (; pch < *ptr; pch ++) {
      divis_ *= 10;
    }
    
    *val += (fract_ << exp) / divis_;
  }
  
 final:
  return res;
}
#endif /* PARAM_SFIX || PARAM_UFIX */

#if PARAM_SFIX
static int stoif(const char **ptr, const char *end, sint_t *val, uint8_t exp){
  char neg = 0;
  
  if (*ptr == end) {
    return param_underflow;
  }
  
  if (**ptr == '-') { /* skip sign */
    neg = 1;
    (*ptr)++;
  }
  
  uint_t *uval = (uint_t*)val;
  
  int res = stouf(ptr, end, uval, exp);
  
  if (neg) {
    *val = -*val;
  }
  
  return res;
}
#endif /* PARAM_SFIX */

#if PARAM_REAL
static int stof(const char **ptr, const char *end, real_t *val){
  char neg = 0;
  
  if (*ptr == end) {
    return param_underflow;
  }
  
  if (**ptr == '-') { /* skip sign */
    neg = 1;
    (*ptr)++;
  }

  uint_t decim_;
  
  const char *pch = *ptr;
  int res = stou(ptr, end, &decim_);
  
  if (*ptr > pch || **ptr == '.') {
    *val = (real_t)decim_;
  }
  
  if (*ptr != end && **ptr == '.') {
    (*ptr)++;
    
    pch = *ptr; /* save point character */
    
    uint_t fract_;
    res = stou(ptr, end, &fract_);
    
    if (*ptr == pch) {
      goto final;
    }
    
    uint_t divis_ = 1;
    for (; pch < *ptr; pch ++) {
      divis_ *= 10;
    }
    
    *val += (real_t)fract_/(real_t)divis_;
  }
  
 final:
  
  if (neg) {
    *val = -*val;
  }
  
  return res;
}
#endif /* PARAM_REAL */

static int utos_dig(char **ptr, char *end, uint_t val, uint_t dig){
  char *p = *ptr, *q = p + dig;
  char t;
  
  do {
    if (p == end) {
      return param_overflow; /* nothing */
    }
    *p++ = (val % 10) + '0';
    val /= 10;
  } while (val > 0);

  while (p < q) {
    if (p == end) {
      return param_overflow; /* nothing */
    }
    *p++ = '0';
  }
  
  q = *ptr;
  *ptr = p--;
  
  while (q < p) { /* reverse digits */
    t = *q;
    *q++ = *p;
    *p-- = t;
  }
  
  return param_success;
}

#define utos(ptr, end, val) utos_dig(ptr, end, val, 0)

#if PARAM_SINT || PARAM_SFIX || PARAM_REAL
static int itos_dig(char **ptr, char *end, sint_t val, uint_t dig){
  if (val < 0 && *ptr != end) {
    *(*ptr)++ = '-';
    val = -val;
  }
  
  return utos_dig(ptr, end, val, dig);
}

#define itos(ptr, end, val) itos_dig(ptr, end, val, 0)
#endif /* PARAM_SINT || PARAM_SFIX || PARAM_REAL */

#if PARAM_UFIX || PARAM_SFIX
static int uftos(char **ptr, char *end, uint_t val, uint8_t exp){
  uint_t ipart = val >> exp;
  uint_t fpart = val & ((1 << exp) - 1);
  
  int res = utos(ptr, end, ipart);
  
  if (res != param_success) {
    return res;
  }
  
  if (fpart == 0) {
    return param_success;
  }
  
  if (*ptr == end) {
    return param_overflow;
  }
  
  *(*ptr)++ = '.';

  for (; fpart > 0; ) {
    if (*ptr == end) {
      return param_overflow;
    }
    fpart *= 10;
    uint_t fdig = fpart >> exp;
    *(*ptr)++ = fdig + '0';
    fpart -= fdig << exp;
  }
  
  return param_success;
}
#endif /* PARAM_UFIX || PARAM_SFIX */

#if PARAM_SFIX
static int iftos(char **ptr, char *end, sint_t val, uint8_t exp){
  if (val < 0) {
    if (*ptr == end) {
      return param_overflow;
    }
    
    *(*ptr)++ = '-';
    val = -val;
  }
  
  return uftos(ptr, end, val, exp);
}
#endif /* PARAM_SFIX */

#if PARAM_REAL
static inline sint_t ftoi(real_t val, uint_t exp){
  for (; exp > 0; exp--) val *= 10;
  real_t afp = val - (sint_t)val;
  return val + (afp < -0.5 ? -1 : afp < 0.5 ? 0 : 1);
}

static int ftos(char **ptr, char *end, real_t val, uint_t exp){
  sint_t ipart = val;
  sint_t fpart = ftoi(val - ipart, exp);

  for (; fpart != 0 && (fpart % 10) == 0; fpart /= 10, exp --);
  
  if (fpart < 0) {
    fpart = -fpart;
  }
  
  int res = itos(ptr, end, ipart);
  
  if (res != param_success) {
    return res;
  }

  if (fpart == 0) {
    return param_success;
  }
  
  if (*ptr == end) {
    return param_overflow;
  }
  
  *(*ptr)++ = '.';
  
  return utos_dig(ptr, end, fpart, exp);
}
#endif /* PARAM_REAL */

int put_quoted_str(char **ptr, char *end, const char *str) {
  /* prepare */
  const char *c = str, *p = *ptr + 2;
  for (; *c != '\0'; ) {
    if (p >= end) {
      return param_overflow;
    }
    switch (*c++) {
    case '\n':
    case '"':
      p += 2;
      break;
    default:
      p ++;
    }
  }
  /* put string data */
  *(*ptr) ++ = '"';
  for (; *str != '\0'; str ++) {
    switch (*str) {
    case '\n':
      *(*ptr) ++ = '\\';
      *(*ptr) ++ = 'n';
      continue;
    case '"':
      *(*ptr) ++ = '\\';
      break;
    }
    *(*ptr) ++ = *str;
  }
  *(*ptr) ++ = '"';
  return param_success;
}

static int stos(char **ptr, char *end, const char *str) {
  if (str == NULL) {
    return param_invalid;
  }
  
  for (; *ptr < end && *str != '\0'; ) {
    *(*ptr)++ = *str++;
  }
  
  if (*str != '\0') {
    return param_overflow;
  }
  
  return param_success;
}

static inline const char *typename(param_flag_t flag){
  switch (flag) {
#define T(t) case param_##t: return #t
    T(none);
#if PARAM_ENUM
    T(enum);
#endif /* PARAM_ENUM */
#if PARAM_UINT
    T(uint);
#endif /* PARAM_UINT */
#if PARAM_SINT
    T(sint);
#endif /* PARAM_SINT */
#if PARAM_UFIX
    T(ufix);
#endif /* PARAM_UFIX */
#if PARAM_SFIX
    T(sfix);
#endif /* PARAM_SFIX */
#if PARAM_REAL
    T(real);
#endif /* PARAM_REAL */
#if PARAM_CSTR
    T(cstr);
#endif /* PARAM_CSTR */
#if PARAM_HBIN
    T(hbin);
#endif /* PARAM_HBIN */
#if PARAM_IPV4
    T(ipv4);
#endif /* PARAM_IPV4 */
#if PARAM_MAC
    T(mac);
#endif /* PARAM_MAC */
#undef T
  }
  return NULL;
}

int param_getstr_type(const param_desc_t *param, char **ptr, char *end) {
  return stos(ptr, end, typename(param_type(param)));
}

int param_getstr_size(const param_desc_t *param, char **ptr, char *end) {
  return utos(ptr, end, param_size(param));
}

#if PARAM_HBIN || PARAM_MAC
static inline char btoh(uint8_t i){
  return i < 10 ? i + '0' : i < 16 ? i - 10 + 'a' : -1;
}

static int htos(char **ptr, char *end, const uint8_t *bin, param_size_t len) {
  if (end - *ptr < len * 2) {
    return param_overflow;
  }
  end = *ptr + len * 2;
  for (; *ptr < end; bin ++) {
    uint8_t val = PARAM_ROM_GET(*bin);
    *(*ptr)++ = btoh((val >> 4) & 0x0f);
    *(*ptr)++ = btoh(val & 0x0f);
  }
  return param_success;
}

static inline uint8_t htob(char h){
  return '0' <= h && h <= '9' ? h - '0' : 'A' <= h && h <= 'F' ? h - 'A' + 10 : 'a' <= h && h <= 'f' ? h - 'a' + 10 : 16;
}

static int stoh(const char **ptr, const char *end, uint8_t *bin, param_size_t len) {
  uint8_t *bin_end = bin + len;
  for (; *ptr < end; bin ++) {
    if (bin >= bin_end) {
      return param_overflow;
    }
    uint8_t h = htob(*(*ptr)++);
    if (h == 16) {
      return param_invalid;
    }
    if (*ptr >= end) {
      return param_underflow;
    }
    uint8_t l = htob(*(*ptr)++);
    if (l == 16) {
      return param_invalid;
    }
    *bin = (h << 4) | l;
  }
  return param_success;
}
#endif /* PARAM_HBIN || PARAM_MAC */

static int getstr(const param_desc_t *param, const param_cell_t *val, char **ptr, char *end){
  if (val != NULL) {
    switch (param_type(param)) {
#if PARAM_ENUM
    case param_enum: {
#if PARAM_NAME && PARAM_TEXT /* symbolic enum */
      uint_t index = 0, count = param_enum_count(param);
      switch (param_size(param)) {
#define ENUM_GET(type)                                      \
        case sizeof(type): {                                \
          uint_t value = PARAM_ROM_GET(*(const type*)val);  \
          for (; index < count; index++)                    \
            if (value == PARAM_ROM_GET(*(const type*)param_enum_value(param, index))) break; \
        } break
#if PARAM_8BIT
        ENUM_GET(uint8_t);
#endif
#if PARAM_16BIT
        ENUM_GET(uint16_t);
#endif
#if PARAM_32BIT
        ENUM_GET(uint32_t);
#endif
#if PARAM_64BIT
        ENUM_GET(uint64_t);
#endif
#undef ENUM_GET
        default: return param_invalid;
      }
      const char *_val = param_enum_text(param, param_none, index);
      return put_quoted_str(ptr, end, _val);
#else /* PARAM_NAME && PARAM_TEXT */ /* numeric enum */
      return param_invalid;
#endif /* PARAM_NAME && PARAM_TEXT */
    }
#endif /* PARAM_ENUM */
#define NUM_GET(type) case sizeof(type): _val = PARAM_ROM_GET(*(const type*)val); break
#if PARAM_SINT
    case param_sint: {
      sint_t _val = 0;
      switch (param_size(param)) {
#if PARAM_8BIT
        NUM_GET(int8_t);
#endif
#if PARAM_16BIT
        NUM_GET(int16_t);
#endif
#if PARAM_32BIT
        NUM_GET(int32_t);
#endif
#if PARAM_64BIT
        NUM_GET(int64_t);
#endif
      }
      return itos(ptr, end, _val);
    }
#endif /* PARAM_SINT */
#if PARAM_UINT
    case param_uint: {
      uint_t _val = 0;
      switch (param_size(param)) {
#if PARAM_8BIT
        NUM_GET(uint8_t);
#endif
#if PARAM_16BIT
        NUM_GET(uint16_t);
#endif
#if PARAM_32BIT
        NUM_GET(uint32_t);
#endif
#if PARAM_64BIT
        NUM_GET(uint64_t);
#endif
      }
      return utos(ptr, end, _val);
    }
#endif /* PARAM_UINT */
#undef NUM_GET
#define NUM_GET(type) case sizeof(type): _val = PARAM_ROM_GET(*(const type*)val); break
#if PARAM_SFIX
    case param_sfix: {
      sint_t _val = 0;
      switch (param_size(param)) {
#if PARAM_8BIT
        NUM_GET(int8_t);
#endif
#if PARAM_16BIT
        NUM_GET(int16_t);
#endif
#if PARAM_32BIT
        NUM_GET(int32_t);
#endif
#if PARAM_64BIT
        NUM_GET(int64_t);
#endif
      }
      return iftos(ptr, end, _val, param_fix_fract(param));
    }
#endif /* PARAM_SFIX */
#if PARAM_UFIX
    case param_ufix: {
      uint_t _val = 0;
      switch (param_size(param)) {
#if PARAM_8BIT
        NUM_GET(uint8_t);
#endif
#if PARAM_16BIT
        NUM_GET(uint16_t);
#endif
#if PARAM_32BIT
        NUM_GET(uint32_t);
#endif
#if PARAM_64BIT
        NUM_GET(uint64_t);
#endif
      }
      return uftos(ptr, end, _val, param_fix_fract(param));
    }
#endif /* PARAM_UFIX */
#undef NUM_GET
#define NUM_GET(type, exp) case sizeof(type): _val = PARAM_ROM_GET(*(const type*)val); return ftos(ptr, end, _val, exp)
#if PARAM_REAL && (PARAM_32BIT || PARAM_64BIT)
    case param_real: {
      real_t _val = 0.0;
      switch (param_size(param)) {
#if PARAM_32BIT
        NUM_GET(float, 8);
#endif
#if PARAM_64BIT
        NUM_GET(double, 16);
#endif /* PARAM_DBL */
      }
    }
#endif /* PARAM_REAL */
#if PARAM_CSTR
    case param_cstr:
      return put_quoted_str(ptr, end, val);
#endif /* PARAM_CSTR */
#if PARAM_HBIN
    case param_hbin: {
      int r;
      *(*ptr)++ = '"';
      if (param_success == (r = htos(ptr, end, val, param_size(param)))) {
        *(*ptr)++ = '"';
      }
      return r;
    }
#endif /* PARAM_HBIN */
#if PARAM_IPV4
    case param_ipv4: {
      int r = param_success;
      uint_t i = 0;

      *(*ptr)++ = '"';
      for (; i < param_size(param) &&
             r == param_success; i++) {
        if (i > 0) {
          if (*ptr == end) {
            return param_overflow;
          }
          *(*ptr)++ = '.';
        }
        r = utos(ptr, end, PARAM_ROM_GET(*(const uint8_t*)val));
        val = (const uint8_t*)val + 1;
      }
      *(*ptr)++ = '"';
      
      return r;
    }
#endif /* PARAM_IPV4 */
#if PARAM_MAC
    case param_mac: {
      int r = param_success;
      uint_t i = 0;

      *(*ptr)++ = '"';
      for (; i < param_size(param) &&
             r == param_success; i++) {
        if (i > 0) {
          if (*ptr == end) {
            return param_overflow;
          }
          *(*ptr)++ = ':';
        }
        r = htos(ptr, end, val, 1);
        val = (const uint8_t*)val + 1;
      }
      *(*ptr)++ = '"';
      
      return r;
    }
#endif /* PARAM_MAC */
    default:
      break;
    }
  } else {
    return stos(ptr, end, "null");
  }
  
  return param_invalid;
}

#if PARAM_CONS
int param_getstr_cons(const param_desc_t *param, param_flag_t field, char **ptr, char *end) {
  return getstr(param, param_cons(param, field), ptr, end);
}
#endif /* PARAM_CONS */

int param_getstr(const param_desc_t *param, char **ptr, char *end) {
  uint8_t val[param_size(param)];
  int res = param_get(param, val);
  
  if (res != param_success) {
    return res;
  }
  
  return getstr(param, val, ptr, end);
}

int param_setstr(const param_desc_t *param, const char **ptr, const char *end) {
  uint8_t val[param_size(param) + 1];
  void *pval = (void*) val;
  
  switch (param_type(param)) {
#if PARAM_ENUM
  case param_enum:
    {
#if PARAM_NAME && PARAM_TEXT /* symbolic enum */
      uint_t count = param_enum_count(param);
      uint_t len = end - *ptr;
      uint_t index = 0;
      
      for (; index < count; index++) {
        const char *name = param_enum_text(param, param_none, index);
        if (name != NULL &&
            len == PARAM_ROM_LEN(name) &&
            0 == PARAM_ROM_CMP(name, *ptr, len)) {
          switch (param_size(param)) {
#define ENUM_SET(type) case sizeof(type): *(type*)pval = PARAM_ROM_GET(*(const type*)param_enum_value(param, index)); goto enum_end;
#if PARAM_8BIT
            ENUM_SET(uint8_t);
#endif
#if PARAM_16BIT
            ENUM_SET(uint16_t);
#endif
#if PARAM_32BIT
            ENUM_SET(uint32_t);
#endif
#if PARAM_64BIT
            ENUM_SET(uint64_t);
#endif
#undef ENUM_SET
          }
        }
      }
    enum_end:
      if (index == count) {
        return param_invalid;
      }
#else /* PARAM_NAME && PARAM_TEXT */ /* numeric enum */
      
#endif /* PARAM_NAME && PARAM_TEXT */
    } break;
#endif /* PARAM_ENUM */
#define NUM_SET(type) case sizeof(type): *(type*)pval = _val; break
#if PARAM_SINT
  case param_sint: {
    sint_t _val = 0;
    int res = stoi(ptr, end, &_val);
    if (res != param_success) {
      return res;
    }
    
    sint_t out = 1 << ((param_size(param) << 3) - 1);
    if (_val < -out) _val = -out;
    if (_val >= out) _val = out - 1;
    
    switch (param_size(param)) {
#if PARAM_8BIT
      NUM_SET(int8_t);
#endif
#if PARAM_16BIT
      NUM_SET(int16_t);
#endif
#if PARAM_32BIT
      NUM_SET(int32_t);
#endif
#if PARAM_64BIT
      NUM_SET(int64_t);
#endif
    }
  } break;
#endif /* PARAM_SINT */
#if PARAM_UINT
  case param_uint: {
    uint_t _val = 0;
    int res = stou(ptr, end, &_val);
    if (res != param_success) {
      return res;
    }
    
    uint_t out = (1 << (param_size(param) << 3));
    
    if (_val >= out) _val = out - 1;
    
    switch (param_size(param)) {
#if PARAM_8BIT
      NUM_SET(uint8_t);
#endif
#if PARAM_16BIT
      NUM_SET(uint16_t);
#endif
#if PARAM_32BIT
      NUM_SET(uint32_t);
#endif
#if PARAM_64BIT
      NUM_SET(uint64_t);
#endif
    }
  } break;
#endif /* PARAM_UINT */
#if PARAM_SFIX
  case param_sfix: {
    sint_t _val = 0;
    int res = stoif(ptr, end, &_val, param_fix_fract(param));
    if (res != param_success) {
      return res;
    }
    
    sint_t out = 1 << ((param_size(param) << 3) - 1);
    if (_val < -out) _val = -out;
    if (_val >= out) _val = out - 1;
    
    switch (param_size(param)) {
#if PARAM_8BIT
      NUM_SET(int8_t);
#endif
#if PARAM_16BIT
      NUM_SET(int16_t);
#endif
#if PARAM_32BIT
      NUM_SET(int32_t);
#endif
#if PARAM_64BIT
      NUM_SET(int64_t);
#endif
    }
  } break;
#endif /* PARAM_SFIX */
#if PARAM_UFIX
  case param_ufix: {
    uint_t _val = 0;
    int res = stouf(ptr, end, &_val, param_fix_fract(param));
    if (res != param_success) {
      return res;
    }
    
    uint_t out = (1 << (param_size(param) << 3));
    
    if (_val >= out) _val = out - 1;
    
    switch (param_size(param)) {
#if PARAM_8BIT
      NUM_SET(uint8_t);
#endif
#if PARAM_16BIT
      NUM_SET(uint16_t);
#endif
#if PARAM_32BIT
      NUM_SET(uint32_t);
#endif
#if PARAM_64BIT
      NUM_SET(uint64_t);
#endif
    }
  } break;
#endif /* PARAM_UFIX */
#if PARAM_REAL
  case param_real: {
    real_t _val = 0.0;
    int res = stof(ptr, end, &_val);
    if (res != param_success) {
      return res;
    }
    
    switch (param_size(param)) {
#if PARAM_32BIT
      NUM_SET(float);
#endif
#if PARAM_64BIT
      NUM_SET(double);
#endif
    }
  } break;
#endif /* PARAM_REAL */
#if PARAM_CSTR
  case param_cstr: {
    param_size_t len = end - *ptr;
    if (len > param_size(param)) {
      return param_overflow;
    }
    PARAM_ROM_READ(pval, *ptr, len);
    val[len] = '\0';
    *ptr = end;
  } break;
#endif /* PARAM_CSTR */
#if PARAM_HBIN
  case param_hbin:
    stoh(ptr, end, pval, param_size(param));
    break;
#endif /* PARAM_HBIN */
#if PARAM_IPV4
  case param_ipv4: {
    int r = param_success;
    uint_t i = 0, v;
    
    for (; i < param_size(param); i++) {
      if (i > 0) {
        if (**ptr == '.') {
          (*ptr)++;
        } else {
          return param_invalid;
        }
      }
      r = stou_max(ptr, end, &v, 255);
      if (r == param_overflow) {
        return r;
      }
      *((uint8_t*)pval + i) = v;
    }
  } break;
#endif /* PARAM_IPV4 */
#if PARAM_MAC
  case param_mac: {
    int r = param_success;
    uint_t i = 0;
    
    for (; i < param_size(param); i++) {
      if (i > 0) {
        if (**ptr == ':') {
          (*ptr)++;
        } else {
          return param_invalid;
        }
      }
#define min(a, b) ((a) < (b) ? (a) : (b))
      r = stoh(ptr, min(end, *ptr + 2), (uint8_t*)pval + i, 1);
      if (r != param_success) {
        return r;
      }
    }
  } break;
#endif /* PARAM_MAC */
  }
  
  return param_set(param, pval);
}
#endif /* PARAM_TYPE */

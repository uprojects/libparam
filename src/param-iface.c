#include "param.h"
#include "param-iface.h"

void param_iface_handle(const param_coll_t *params, const param_cell_t *iptr, param_size_t ilen, param_cell_t *optr, param_size_t *olen) {
  const param_req_t *req = (const param_req_t *)iptr;
  param_res_t *res = (param_res_t*)optr;
  param_size_t cnt = PARAM_ROM_GET(params->count);
  
  if (ilen < param_field_size(param_req_t, op)) {
    res->status = param_error;
    *olen = param_field_size(param_res_t, status);
    return;
  }
  
  param_size_t size;
  
  switch (req->op) {
  case param_op_list: size = param_header_size(param_req_t, list); break;
  case param_op_desc: size = param_header_size(param_req_t, desc); break;
  case param_op_gets: size = param_header_size(param_req_t, gets); break;
  case param_op_sets: size = param_header_size(param_req_t, sets); break;
  default: /* unsupported operation */
    res->status = param_error;
    *olen = param_field_size(param_res_t, status);
    return;
  }
  
  if (ilen < size) { /* incomplete header */
    res->status = param_invalid;
    *olen = param_field_size(param_res_t, status);
    return;
  }
  
  switch (req->op) {
  case param_op_gets:
    size += req->gets.cnt * param_field_size(param_req_t, gets.idx[0]);
    break;
  case param_op_sets:
    size += req->sets.cnt * param_field_size(param_req_t, sets.idx[0]);
    break;
  }
  
  if (ilen < size) { /* incomplete header */
    res->status = param_invalid;
    *olen = param_field_size(param_res_t, status);
    return;
  }
  
  /* check indexes */
  const param_idx_t *idx_p = NULL, *idx_pe = NULL;
  
  switch (req->op) {
  case param_op_desc: idx_p = &req->desc.idx; idx_pe = idx_p + 1; break;
  case param_op_gets: idx_p = req->gets.idx; idx_pe = idx_p + req->gets.cnt; break;
  case param_op_sets: idx_p = req->sets.idx; idx_pe = idx_p + req->sets.cnt; break;
  }

  /* save parameter indexes */
  param_idx_t idxs[idx_pe - idx_p];
  idx_pe = idxs + (idx_pe - idx_p);
  {
    param_idx_t *idx_pi = idxs;
    for (; idx_p != idx_pe; ) {
      if (*idx_p >= cnt) {
        /* parameter not found */
        res->status = param_error;
        *olen = param_field_size(param_res_t, status);
        return;
      }
      *idx_pi++ = *idx_p++;
    }
  }
  
  /* check request body */
  switch (req->op) {
  case param_op_sets:
    idx_p = idxs;
    for (; idx_p != idx_pe; )
      size += param_size(&params->param[*idx_p++]);
  }

  if (ilen < size) { /* incomplete body */
    res->status = param_invalid;
    *olen = param_field_size(param_res_t, status);
    return;
  }
  
  switch (req->op) { /* calculate response size */
  case param_op_list:
    size = param_header_size(param_res_t, list);
    break;
  case param_op_desc:
    size = param_header_size(param_res_t, desc);
#if PARAM_CONS || PARAM_TEXT
    {
      const param_desc_t *param = &params->param[idxs[0]];
      param_flag_t field, flags = param_flag(param);
#endif
#if PARAM_CONS
      if (param->cons != NULL) {
        field = param_cons_init;
#if PARAM_CSTR
        if (param_type(param) != param_cstr) {
#endif
          for (; field < param_cons_mask; field <<= 1) {
            if (flags & field) {
              size += param_size(param);
            }
          }
#if PARAM_CSTR
        } else {
#if PARAM_DEF
          const char *const s = param_cons(param, param_def);
          if (s) {
            size += strlen(s) + 1;
          }
#endif
        }
#endif
      }
#endif
#if PARAM_TEXT
      const char *const *text = PARAM_ROM_GET(param->text);
      if (param->text != NULL) {
        field = param_text_init;
        for (; field < param_text_mask; field <<= 1) {
          if (flags & field) {
            size += PARAM_ROM_LEN(*text) + 1;
            text ++;
          }
        }
      }
#endif
#if PARAM_CONS || PARAM_TEXT
    }
#endif
    break;
  case param_op_gets:
    size = param_header_size(param_res_t, gets);
    idx_p = idxs;
    for (; idx_p != idx_pe; ) {
#if PARAM_CSTR
      if (param_type(&params->param[*idx_p++]) != param_cstr) {
#endif
        size += param_size(&params->param[*idx_p++]);
#if PARAM_CSTR
      } else {
        //size += strlen();
      }
#endif
    }
    break;
  case param_op_sets:
    size = param_header_size(param_res_t, sets);
    break;
  }
  
  if (*olen < size) { /* response couldn't be placed in output buffer */
    res->status = param_overflow;
    *olen = param_field_size(param_res_t, status);
    return;
  }
  
  *olen = size; /* set response length */
  
  switch (req->op) {
  case param_op_list:
    res->list.cnt = cnt;
    break;
  case param_op_desc: {
    const param_desc_t *param = &params->param[idxs[0]];
    res->desc.flag = param_flag(param);
    res->desc.size = param_size(param);
#if PARAM_CONS || PARAM_NAME || PARAM_TEXT
    uint8_t *data = res->desc.addon;
#endif
#if PARAM_CONS
    if (param->cons != NULL) {
      param_flag_t field = param_text_init;
      const uint8_t *pval = param->cons;
      for (; field < param_text_mask; field <<= 1) {
        if (param_flag(param) & field) {
          memcpy(data, pval, param_size(param));
          pval += param_size(param);
          data += param_size(param);
        }
      }
    }
#endif
#if PARAM_NAME
    if (param_name(param) != NULL) {
      param_size_t len = PARAM_ROM_LEN(param_name(param));
      PARAM_ROM_READ(data, param_name(param), len);
      data += len;
    } else {
      *data ++ = '\0';
    }
#endif
#if PARAM_TEXT
    if (PARAM_ROM_GET(param->text) != NULL) {
      param_flag_t field = param_text_init;
      const char *const *text = PARAM_ROM_GET(param->text);
      for (; field < param_text_mask; field <<= 1) {
        if (param_flag(param) & field) {
          param_size_t len = PARAM_ROM_LEN(*text);
          PARAM_ROM_READ(data, *text, len);
          data += len;
          text ++;
        }
      }
    }
#endif
  } break;
  case param_op_gets: {
    uint8_t *data = res->gets.value;
    for (idx_p = idxs; idx_p != idx_pe; ) {
      param_get(&params->param[*idx_p], data);
      data += param_size(&params->param[*idx_p++]);
    }
  } break;
  case param_op_sets: {
    const uint8_t *data = (const uint8_t*)req->sets.idx + req->sets.cnt * param_field_size(param_req_t, sets.idx[0]);
    for (idx_p = idxs; idx_p != idx_pe; ) {
      param_set(&params->param[*idx_p], data);
      data += param_size(&params->param[*idx_p++]);
    }
  } break;
  }
  
  res->status = param_success;
  return;
}

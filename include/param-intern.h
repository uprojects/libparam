#ifndef __PARAM_INTERN_H__
#define __PARAM_INTERN_H__ "param-intern.h"

#if PARAM_NAME
/**
 * @brief The unique identifier of parameter.
 */
typedef uint32_t param_uuid_t;

/**
 * @brief Get unique identifier of parameter.
 */
param_uuid_t param_uuid(const param_desc_t *param);
#endif

/**
 * @brief Raw get value of parameter.
 *
 * @param param The target parameter descriptor.
 * @param val The pointer to destination value buffer.
 * @return The success status of operation or error code (@see param_res).
 */
int param_raw_get(const param_desc_t *param, param_cell_t *val);

/**
 * @brief Raw set value of parameter.
 *
 * @param param The target parameter descriptor.
 * @param value The pointer to source value buffer.
 * @return The success status of operation or error code (@see param_res).
 *
 * The value will be fixed using @p min, @p max and @p step fields.
 * If the @p value is NULL, then default value is used (if specified).
 */
int param_raw_set(const param_desc_t *param, const param_cell_t *val);

#if PARAM_PERS
#if !PARAM_NAME
#error "Actually you need uuid feature. Please use parameter names if you really need persistent storage feature."
#endif

/**
 * @brief Save actual value of parameter for future usage.
 *
 * @param param The target parameter descriptor.
 * @return The status of operation (@see param_res).
 */
int param_save(const param_desc_t *param);

/**
 * @brief Load actual value of parameter.
 *
 * @param param The target parameter descriptor.
 * @return The status of operation (@see param_res).
 */
int param_load(const param_desc_t *param);

/**
 * @brief Save actual value of listed parameters for future usage.
 *
 * @param params The parameter descriptors group.
 * @param count The count of parameters in group.
 * @return The status of operation (@see param_res).
 */
int param_save_all(const param_coll_t *params);

/**
 * @brief Load actual value of listed parameters.
 *
 * @param params The parameter descriptors group.
 * @param count The count of parameters in group.
 * @return The status of operation (@see param_res).
 */
int param_load_all(const param_coll_t *params);

#endif/*PARAM_PERS*/

#endif/*__PARAM_INTERN_H__*/

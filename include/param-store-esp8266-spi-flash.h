#include "sdk/flash.h"

extern const unsigned _blank_start;
extern const unsigned _blank_end;

static const addr_t store_start = (addr_t)&_blank_start;
static const addr_t store_end = (addr_t)&_blank_end;

#define store_align 4
#define store_sector (4 << 10)

static inline void store_lock(void) {}
static inline void store_unlock(void) {}

static inline void store_erase(addr_t addr) {
  spi_flash_erase_block(addr);
}

static inline void store_read(addr_t addr, param_cell_t *ptr, param_size_t len) {
  spi_flash_read(addr, ptr, len);
}

static inline void store_write(addr_t addr, const param_cell_t *ptr, param_size_t len) {
  spi_flash_write(addr, (uint32_t*)ptr, len);
}

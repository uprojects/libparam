#include HWDEF_HEADER

#define HWDEF_REGION_(region)              \
  static const addr_t store_start =        \
    (addr_t)&_##region##_start;            \
  static const addr_t store_end =          \
    (addr_t)&_##region##_end

#define HWDEF_REGION(region)               \
  HWDEF_REGION_(region)

HWDEF_REGION(HWDEF_STORE);

#define store_align FLASH_WORD_SIZE
#define store_sector (FLASH_PAGE_SIZE << 10)

static inline void store_lock(void) {
  flash_lock((void*)store_start);
}

static inline void store_unlock(void) {
  flash_unlock((void*)store_start);
}

static inline void store_erase(addr_t addr) {
  flash_erase((void*)addr, store_sector);
}

static inline void store_read(addr_t addr, param_cell_t *ptr, param_size_t len) {
  memcpy(ptr, (const void*)addr, len);
}

static inline void store_write(addr_t addr, const param_cell_t *ptr, param_size_t len) {
  flash_write((void*)addr, ptr, len);
}

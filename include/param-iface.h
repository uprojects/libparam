#ifndef __PARAM_IFACE_H__
#define __PARAM_IFACE_H__ "param-iface.h"

/**
 * @brief The interface operations.
 */
enum {
  /**
   * @brief Get list of all parameters.
   */
  param_op_list = 'l',
  /**
   * @brief Get parameter descriptor.
   */
  param_op_desc = 'd',
  /**
   * @brief Get parameter's values.
   */
  param_op_gets = 'g',
  /**
   * @brief Set parameter's values.
   */
  param_op_sets = 's',
};

typedef uint16_t param_idx_t;

/**
 * @brief The request header.
 */
typedef struct {
  /**
   * @brief The request operation.
   */
  uint8_t op;
  uint8_t __pad1[3];
  /**
   * @brief The request options dependent from operation.
   */
  union {
    /**
     * @brief The list request options.
     */
    struct {
      /**
       * @brief The pointer to end of request header.
       */
      uint8_t end[0];
    } list;
    /**
     * @brief The desc request options.
     */
    struct {
      /**
       * @brief The index of parameter.
       */
      param_idx_t idx;
      /**
       * @brief The pointer to end of request header.
       */
      uint8_t end[0];
    } desc;
    /**
     * @brief The gets and sets requests options.
     */
    struct {
      /**
       * @brief The count of parameters to get or set.
       */
      param_idx_t cnt;
      uint8_t __pad2[2];
      /**
       * @brief The pointer to indexes of parameters.
       */
      param_idx_t idx[0];
      /**
       * @brief The pointer to end of request header.
       */
      uint8_t end[0];
    } gets, sets;
  };
} param_req_t;

#define param_field_size(type, name) sizeof(((type*)NULL)->name)

#ifdef __builtin_offsetof
#  define param_header_size(type, kind) __builtin_offsetof(type, kind.end)
#else
#  define param_header_size(type, kind) ((size_t)((type*)NULL)->kind.end)
#endif

/**
 * @brief The response header.
 */
typedef struct {
  /**
   * @brief The operation status.
   */
  uint8_t status;
  uint8_t __pad1[3];
  /**
   * @brief The response options dependent from operation.
   */
  union {
    /**
     * @brief The list response options.
     */
    struct {
      /**
       * @brief The count of parameters.
       */
      param_idx_t cnt;
      uint8_t __pad2[2];
      /**
       * @brief The pointer to end of response header.
       */
      uint8_t end[0];
    } list;
    /**
     * @brief The desc response options.
     */
    struct {
      /**
       * @brief The set of parameter's flags and options.
       */
      param_flag_t flag;
      /**
       * @brief The size of parameter value.
       */
      param_size_t size;
      /**
       * @brief The pointer to additional data.
       */
      uint8_t addon[0];
      /**
       * @brief The pointer to end of response header.
       */
      uint8_t end[0];
    } desc;
    /**
     * @brief The list response options.
     */
    struct {
      /**
       * @brief The pointer to values.
       */
      uint8_t value[0];
      /**
       * @brief The pointer to end of response header.
       */
      uint8_t end[0];
    } gets;
    /**
     * @brief The set response options.
     */
    struct {
      /**
       * @brief The pointer to end of response header.
       */
      uint8_t end[0];
    } sets;
  };
} param_res_t;

/**
 * @brief The string representation in packet.
 */
typedef struct {
  param_size_t len;
  char str[0];
} param_str_t;

/**
 * @brief Handles parameter interface on device.
 *
 * @param iptr The pointer to input buffer.
 * @param ilen The length of received data in buffer.
 * @param optr The pointer to output buffer.
 * @param olen The max length of output buffer (initial) and length of data to send.
 */
void param_iface_handle(const param_coll_t *params,
                        const param_cell_t *iptr,
                        param_size_t ilen,
                        param_cell_t *optr,
                        param_size_t *olen);

#endif/*__PARAM_IFACE_H__*/

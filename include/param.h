#ifndef __PARAM_H__
#define __PARAM_H__ "param.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#ifdef PARAM_CONFIG
#include PARAM_CONFIG
#endif

#define PARAM_DBG_NONE 0x0000
#define PARAM_DBG_CONF 0x1000

#ifndef PARAM_DBG
#define PARAM_DBG PARAM_DBG_NONE
#endif

#ifndef PARAM_8BIT
/**
 * @brief Enable/Disable double (8-bit) atomic types of parameter.
 */
#define PARAM_8BIT 0
#endif

#ifndef PARAM_16BIT
/**
 * @brief Enable/Disable double (16-bit) atomic types of parameter.
 */
#define PARAM_16BIT 0
#endif

#ifndef PARAM_32BIT
/**
 * @brief Enable/Disable double (32-bit) atomic types of parameter.
 */
#define PARAM_32BIT 0
#endif

#ifndef PARAM_64BIT
/**
 * @brief Enable/Disable double (64-bit) atomic types of parameter.
 */
#define PARAM_64BIT 0
#endif

#ifndef PARAM_ENUM
/**
 * @brief Enable/Disable enumeration type of parameter.
 */
#define PARAM_ENUM 0
#endif

#ifndef PARAM_UINT
/**
 * @brief Enable/Disable unsigned integer types of parameter.
 */
#define PARAM_UINT 0
#endif

#ifndef PARAM_SINT
/**
 * @brief Enable/Disable signed integer types of parameter.
 */
#define PARAM_SINT 0
#endif

#ifndef PARAM_UFIX
/**
 * @brief Enable/Disable unsigned fixed point types of parameter.
 */
#define PARAM_UFIX 0
#endif

#ifndef PARAM_SFIX
/**
 * @brief Enable/Disable signed fixed point types of parameter.
 */
#define PARAM_SFIX 0
#endif

#ifndef PARAM_REAL
/**
 * @brief Enable/Disable real types of parameter.
 */
#define PARAM_REAL 0
#endif

#if !(PARAM_8BIT || PARAM_16BIT || PARAM_32BIT || PARAM_64BIT)
#undef PARAM_ENUM
#define PARAM_ENUM 0
#undef PARAM_UINT
#define PARAM_UINT 0
#undef PARAM_SINT
#define PARAM_SINT 0
#undef PARAM_UFIX
#define PARAM_UFIX 0
#undef PARAM_SFIX
#define PARAM_SFIX 0
#endif

#if !(PARAM_32BIT || PARAM_64BIT)
#undef PARAM_REAL
#define PARAM_REAL 0
#endif

#define PARAM_ATOM (PARAM_ENUM || PARAM_UINT || PARAM_SINT || PARAM_UFIX || PARAM_SFIX || PARAM_REAL)

#ifndef PARAM_CSTR
/**
 * @brief Enable/Disable null-terminated string type of parameter.
 */
#define PARAM_CSTR 0
#endif

#ifndef PARAM_HBIN
/**
 * @brief Enable/Disable hexademical binary buffer type of parameter.
 */
#define PARAM_HBIN 0
#endif

#ifndef PARAM_IPV4
/**
 * @brief Enable/Disable IPv4 address type.
 */
#define PARAM_IPV4 0
#endif

#ifndef PARAM_MAC
/**
 * @brief Enable/Disable arp mac address type.
 */
#define PARAM_MAC 0
#endif

#define PARAM_COMP (PARAM_CSTR || PARAM_HBIN || PARAM_IPV4 || PARAM_MAC)

#define PARAM_TYPE (PARAM_ATOM || PARAM_COMP)

#ifndef PARAM_FIX
/**
 * @brief Enable/Disable fix value of parameter on set.
 */
#define PARAM_FIX 0
#endif

#ifndef PARAM_DEF
/**
 * @brief Enable/Disable default value of parameter.
 */
#define PARAM_DEF 0
#endif

#ifndef PARAM_MIN
/**
 * @brief Enable/Disable minimum value of parameter.
 */
#define PARAM_MIN 0
#endif

#ifndef PARAM_MAX
/**
 * @brief Enable/Disable maximum value of parameter.
 */
#define PARAM_MAX 0
#endif

#ifndef PARAM_STP
/**
 * @brief Enable/Disable stepping value of parameter.
 */
#define PARAM_STP 0
#endif

#define PARAM_CONS (PARAM_DEF || PARAM_MIN || PARAM_MAX || PARAM_STP || PARAM_ENUM || PARAM_UFIX || PARAM_SFIX)

#ifndef PARAM_UNIT
/**
 * @brief Enable/Disable parameter unit text field.
 */
#define PARAM_UNIT 0
#endif

#ifndef PARAM_INFO
/**
 * @brief Enable/Disable parameter info text field.
 */
#define PARAM_INFO 0
#endif

#ifndef PARAM_ITEM
/**
 * @brief Enable/Disable parameter item text field.
 */
#define PARAM_ITEM 0
#endif

#ifndef PARAM_HINT
/**
 * @brief Enable/Disable parameter hint text field.
 */
#define PARAM_HINT 0
#endif

#define PARAM_TEXT (PARAM_UNIT || PARAM_INFO || PARAM_ITEM || PARAM_HINT)

#ifndef PARAM_PERS
/**
 * @brief Enable/Disable persistent parameters.
 */
#define PARAM_PERS 0
#endif

#ifndef PARAM_NAME
#if PARAM_PERS
#if PARAM_DBG & PARAM_DBG_CONF
#warning "Flag PARAM_NAME needed for PARAM_PERS and will be enabled."
#endif
#define PARAM_NAME 1
#else
#define PARAM_NAME 0
#endif
#endif

#ifndef PARAM_VIRT
/**
 * @brief Enable/Disable virtual parameters.
 */
#define PARAM_VIRT 0
#endif

#ifndef PARAM_HASH
/**
 * @brief Param hash function to use (djb2 or sdbm).
 */
#define PARAM_HASH djb2
#endif

#ifndef PARAM_FMUL
/**
 * @brief Use multiplication (*) to calculate hash.
 */
#define PARAM_FMUL 1
#endif

/**
 * @brief The type used for representing parameter options.
 */
typedef uint16_t param_flag_t;

/**
 * @brief The type used for size values.
 */
typedef uint16_t param_size_t;

/**
 * @brief The type used for pointing to memory cell.
 */
typedef void param_cell_t;

enum {
/**
 * @brief The bit mask of type of parameter.
 *
 * The actual C-type of parameter depends from value of field @p size in parameter descriptor.
 * For example, if type is @p param_sint, this may be one of:
 * @li int8_t (size=1)
 * @li int16_t (size=2)
 * @li int32_t (size=4)
 * @li int64_t (size=8)
 */
  param_type_mask = 0x000f,

/**
 * @brief The none type.
 *
 * That type is used for non-value parameters.
 * For example, you can define caption of parameter sections with it.
 */
  param_none = 0x0000,
  
#if PARAM_ENUM
/**
 * @brief The enumeration type.
 *
 * The enumeration type requires @p param_max constraint and optional @p param_min constraint.
 * Also needed separate string description for each enum option after all other strings.
 * Only unsigned integer enum values supported.
 */
  param_enum = 0x0001,
#endif

#if PARAM_UINT
  /**
   * @brief The unsigned integer number (uint8/16/32 bits depending from size).
   */
  param_uint = 0x0002,
#endif

#if PARAM_SINT
  /**
   * @brief The signed integer number (uint8/16/32 bits depending from size).
   */
  param_sint = 0x0003,
#endif

#if PARAM_UFIX
  /**
   * @brief The unsigned fixed point number (uint8/16/32 bits depending from size).
   */
  param_ufix = 0x0004,
#endif

#if PARAM_SFIX
  /**
   * @brief The signed fixed point number (uint8/16/32 bits depending from size).
   */
  param_sfix = 0x0005,
#endif

#if PARAM_REAL
/**
 * @brief The real number (float/double depending from size).
 */
  param_real = 0x0006,
#endif

#if PARAM_COMP
/**
 * @brief The parameter is non-atomic (string or binary array).
 */
  param_comp_mask = 0x0008,
#endif

#if PARAM_CSTR
/**
 * @brief The null terminated string limited length.
 */
  param_cstr = 0x0009,
#endif

#if PARAM_HBIN
  /**
   * @brief The binary array with static length.
   */
  param_hbin = 0x000a,
#endif

#if PARAM_IPV4
  /**
   * @brief The IPv4 address.
   */
  param_ipv4 = 0x000b,
#endif

#if PARAM_MAC
  /**
   * @brief The mac address.
   */
  param_mac = 0x000c,
#endif

/**
 * @brief The bit mask of access mode.
 */
  param_mode_mask = 0x00f0,

/**
 * @brief The parameter value is readable by client.
 *
 * Even when parameter isn't readable by client, the application can get the value of parameter.
 */
  param_getable = 0x0010,

/**
 * @brief The parameter value writable by client.
 *
 * Even when parameter isn't writable by client, the application can set the value of parameter.
 */
  param_setable = 0x0020,

/**
 * @brief The parameter value is permanently stored in flash or eeprom.
 *
 * The application or client can save the actual parameter value for future usage.
 * On each startup the saved values of parameters will be loaded from storage.
 */
  param_persist = 0x0040,

#if PARAM_VIRT
/**
 * @brief The parameter is virtual (actual value is accessible using getter/setter).
 *
 * If parameter is virtual, the getter and setter functions will be used for accessing to value of parameter instead of direct access by pointer.
 */
  param_virtual = 0x0080,
#endif

/**
 * @brief The bit mask of constraint flags.
 */
  param_cons_mask = 0x0f00,

/**
 * @brief The first constraint flag.
 */
  param_cons_init = 0x0100,

/**
 * @brief The offset pf constraint flags.
 */
  param_cons_left = 8,

#if PARAM_DEF
/**
 * @brief The parameter has default value.
 */
  param_def = 0x0100,
#endif
#if PARAM_MIN
/**
 * @brief The parameter has minimal value.
 */
  param_min = 0x0200,
#endif
#if PARAM_MAX
/**
 * @brief The parameter has maximal value.
 */
  param_max = 0x0400,
#endif
#if PARAM_STP
/**
 * @brief The parameter has step value.
 */
  param_stp = 0x0800,
#endif

/**
 * @brief The bit mask of text fields flags.
 */
  param_text_mask = 0xf000,

/**
 * @brief The first text field flag.
 */
  param_text_init = 0x1000,

/**
 * @brief The offset of text fields flags.
 */
  param_text_left = 12,
  
#if PARAM_UNIT
/**
 * @brief The parameter has unit text field.
 */
  param_unit = 0x1000,
#endif
#if PARAM_INFO
/**
 * @brief The parameter has description text field.
 */
  param_info = 0x2000,
#endif
#if PARAM_ITEM
/**
 * @brief The parameter has hint text field.
 */
  param_item = 0x4000,
#endif
#if PARAM_HINT
/**
 * @brief The parameter has hint text field.
 */
  param_hint = 0x8000,
#endif
};

/**
 * @brief The status of operation.
 */
enum param_res {
  param_success = 0,
  param_error = 1,
  param_invalid = 2,
  param_overflow = 3,
  param_underflow = 4,
};

typedef struct param_desc param_desc_t;

/**
 * @brief The virtual parameter value getter.
 *
 * @param The parameter descriptor.
 * @param The pointer to put actual value.
 * @return The success or error (@see param_res).
 */
typedef int param_get_t(const param_desc_t *param, param_cell_t *value);

/**
 * @brief The virtual parameter value getter.
 *
 * @param The parameter descriptor.
 * @param The pointer to new value.
 * @return The success or error (@see param_res).
 */
typedef int param_set_t(const param_desc_t *param, const param_cell_t *value);

/**
 * @brief The virtual parameter access methods table.
 *
 * The methods for accessing to value of virtual parameters.
 */
typedef struct {
  /**
   * @brief The parameter value getter.
   *
   * Even if parameter isn't readable by client, the application can use getter for retrieve the value of parameter internally.
   */
  param_get_t *get;
  /**
   * @brief The parameter value setter.
   *
   * Even if parameter isn't writable by client, the application can use setter for update the value of parameter internally.
   */
  param_set_t *set;
} param_vmt_t;

/**
 * @brief The parameter descriptor.
 *
 * At least 16 bytes of program memory (i.e. flash) is used for each parameter descriptor when all features is used.
 * In addition program memory is used by parameter's strings, constraints and getter/setter functions.
 */
struct param_desc {
#if PARAM_NAME
  /**
   * @brief The textual name of parameter:
   */
  const char *name;
#endif
  /**
   * @brief The set of parameter flags and options such as:
   * 
   * @li value type
   * @li access mode
   * @li use-case flags
   */
  param_flag_t flag;
  /**
   * @brief The size of parameter in number of bytes.
   */
  param_size_t size;
  /**
   * @brief The pointer to parameter value or accessor table.
   *
   * This points to the actual parameter value or NULL if unused.
   * When parameter is virtual (@p mode field of parameter descriptor has the @p param_virtual flag), then this points to the parameter getter/setter table.
   */
  param_cell_t *cell;
#if PARAM_CONS
  /**
   * @brief The pointer to parameter constraints array.
   *
   * You must set flags in @p flag field according to defined constraint values.
   * The predefined order of constraint values: def, min, max, stp.
   *
   * For example:
   *
   * @code{.c}
   * static const int p1_cons[] = {
   *   default_value
   *   minimum_value
   * };
   *
   * static const param_desc_t p1_desc = {
   *   ...
   *   param_sint, param_def | param_min ...,
   *   sizeof(int),
   *   ...
   *   p1_cons
   * };
   *
   * static const float p2_cons[] = {
   *   minimum_value,
   *   step_value,
   * };
   *
   * static const param_desc_t p1_desc = {
   *   ...
   *   param_real | param_min | param_stp ...,
   *   sizeof(float),
   *   ...
   *   p1_cons
   * };
   * @endcode
   */
  const param_cell_t *cons;
#endif
#if PARAM_TEXT
  /**
   * @brief The pointer to array of text entries.
   *
   * The parameter may have some number of text entries, such as name, unit, description, menu/hint and options of enumeration.
   * Quite similar to @p cons field, you must set flags in @p flag field according to defined text entries.
   * The predefined order of text entries: name, unit, desc, hint and some number of option strings when the enumeration type is used.
   *
   * For example:
   *
   * @code{.c}
   * typedef enum {
   *   control_off,
   *   control_auto,
   *   control_manual,
   * } control_mode_t;
   *
   * static const control_mode_t control_mode_cons[] = {
   *   control_auto,   // the option which will be used by default
   *   control_off,    // the first available option of enumeration
   *   control_auto,   // the second available option of enumeration
   *   control_manual, // the third available option of enumeration
   * };
   *
   * static const char *control_mode_text[] = {
   *   "control_mode",            // the machine readable name of parameter
   *   "The control mode switch", // the human readable description of parameter
   *
   *   "off",                     // the machine readable name of first option of enumeration
   *   "The disabled state",      // the human readable description of first option of enumeration
   *   "auto",
   *   "The auto control",        // the second option of enumeration
   *   "manual",
   *   "The manual control",      // the last option of enumeration
   * };
   *
   * control_mode_t control_mode;
   *
   * static const param_desc_t control_mode_desc = {
   *   ...
   *   param_enum, param_def | param_info ...,
   *   sizeof(control_mode), &control_mode,
   *   ...
   *   control_mode_cons,
   *   control_mode_text,
   *   ...
   * };
   * @endcode
   */
  const char *const *text;
#endif
};

#if defined(PARAM_ROM_ALIGN) && PARAM_ROM_ALIGN > 1
#define PARAM_ROM_READ param_rom_read
#define PARAM_ROM_VAR_(num) __val##num##__
#define PARAM_ROM_GET_(cell, num) ({              \
      typeof(cell) PARAM_ROM_VAR_(num);           \
      PARAM_ROM_READ((void*)&PARAM_ROM_VAR_(num), \
                     &(cell),                     \
                     sizeof(cell));               \
      PARAM_ROM_VAR_(num);                        \
    })
#define PARAM_ROM_GET(cell) PARAM_ROM_GET_(cell, __COUNTER__)
#define PARAM_ROM_CMP param_rom_cmp
#define PARAM_ROM_LEN param_rom_len
void param_rom_read(void *ptr, const void *addr, size_t len);
int param_rom_cmp(const void *ptr, const void *addr, size_t len);
param_size_t param_rom_len(const char *addr);
#endif

#ifndef PARAM_ROM_GET
/**
 * @brief The read-only memory getter.
 *
 * Get atomic 16-32 bit value from read-only memory.
 */
#define PARAM_ROM_GET(cell) (cell)
#endif

#ifndef PARAM_ROM_READ
/**
 * @brief The read-only memory reader.
 *
 * Copy piece of data from read-only memory.
 */
#define PARAM_ROM_READ(ptr, addr, len) memcpy(ptr, addr, len)
#endif

#ifndef PARAM_ROM_CMP
/**
 * @brief The read-only memory comparator.
 *
 * Compare piece of data from read-only memory.
 */
#define PARAM_ROM_CMP(ptr, addr, len) memcmp(ptr, addr, len)
#endif

#ifndef PARAM_ROM_LEN
/**
 * @brief The read-only string length.
 *
 * Get length of null-terminated string in read-only memory.
 */
#define PARAM_ROM_LEN(addr) strlen(addr)
#endif

#if PARAM_NAME
/**
 * @brief Get parameter name.
 */
#define param_name(param) (PARAM_ROM_GET((param)->name))
#endif

/**
 * @brief Get parameter flag.
 */
#define param_flag(param) PARAM_ROM_GET((param)->flag)

/**
 * @brief Get parameter type.
 */
#define param_type(param) (param_flag(param) & param_type_mask)

/**
 * @brief Get parameter size.
 */
#define param_size(param) PARAM_ROM_GET((param)->size)

/**
 * @brief Get parameter cell.
 */
#define param_cell(param) PARAM_ROM_GET((param)->cell)

/**
 * @brief Check parameter is atomic or not.
 */
#if PARAM_COMP
#define param_atomic(param) (!(param_flag(param) & param_comp_mask))
#else
#define param_atomic(param) 1
#endif

#if PARAM_CONS
/**
 * @brief Get pointer to value of constraint by flag.
 *
 * @param param The target parameter description.
 * @param field The required constraint field.
 * @return The pointer to constraint field value or NULL if field isn't set.
 */
const param_cell_t *param_cons(const param_desc_t *param, param_flag_t field);
#endif

#if PARAM_TEXT
/**
 * @brief Get value of text field by flag.
 *
 * @param param The target parameter description.
 * @param field The required text field flag.
 * @return The pointer to text entry or NULL if field isn't set.
 */
const char *param_text(const param_desc_t *param, param_flag_t field);
#endif

#if PARAM_ENUM
/**
 * @brief Get the number of enumeration options.
 *
 * @param param The target parameter description.
 * @return The number of options.
 */
param_size_t param_enum_count(const param_desc_t *param);

/**
 * @brief Get the value of enumeration option.
 *
 * @param param The target parameter description.
 * @param index The required enum option index.
 * @return The pointer to option value.
 */
static inline const param_cell_t *
param_enum_value(const param_desc_t *param, param_size_t index) {
  return param_cons(param, 1 + index);
}

#if PARAM_TEXT
/**
 * @brief Get enumeration option text.
 *
 * @param param The target parameter description.
 * @param field The required text field flag.
 * @param index The required enum option index.
 * @return The pointer to text entry.
 */
const char *param_enum_text(const param_desc_t *param, param_flag_t field, param_size_t index);
#endif
#endif

#if PARAM_UFIX || PARAM_SFIX
/**
 * @brief Get the number of the fraction part bits.
 *
 * @param param The target parameter description.
 * @return The number of the fraction part bits.
 */
param_size_t param_fix_fract(const param_desc_t *param);
#endif

/**
 * @brief Get value of parameter.
 *
 * @param param The target parameter descriptor.
 * @param val The pointer to destination value buffer.
 * @return The success status of operation or error code (@see param_res).
 */
int param_get(const param_desc_t *param, param_cell_t *val);

/**
 * @brief Set value of parameter.
 *
 * @param param The target parameter descriptor.
 * @param value The pointer to source value buffer.
 * @return The success status of operation or error code (@see param_res).
 *
 * The value will be fixed using @p min, @p max and @p step fields.
 * If the @p value is NULL, then default value is used (if specified).
 */
int param_set(const param_desc_t *param, const param_cell_t *val);

/**
 * @brief Collection, which contains some set parameter's descriptors.
 */
typedef struct {
  /**
   * @brief The number of parameter's descriptors in collection.
   */
  param_size_t count;
  /**
   * @brief The parameter's descriptors.
   */
  const param_desc_t *param;
} param_coll_t;

#ifndef PARAM_ROM_ATTR
/**
 * @brief The read-only memory attribute.
 *
 * Use this to relocate definitions in special sections like flash memory.
 */
#define PARAM_ROM_ATTR
#endif

/**
 * @brief Define collection of parameters.
 */
#define PARAM_COLL(name, ...)                                       \
  static const param_desc_t name##_desc[] PARAM_ROM_ATTR = {        \
    __VA_ARGS__ };                                                  \
  const param_coll_t name PARAM_ROM_ATTR = { sizeof(name##_desc) /  \
                                             sizeof(param_desc_t),  \
                                             name##_desc }

typedef uint8_t param_num_t;
#define param_num_none 255

static inline param_num_t param_num(const param_coll_t *params, const param_desc_t *param) {
  return param ? param - PARAM_ROM_GET(params->param) : PARAM_ROM_GET(params->count);
}

static inline const param_desc_t *param_desc(const param_coll_t *params, param_num_t num) {
  return num < PARAM_ROM_GET(params->count) ? PARAM_ROM_GET(params->param) + num : NULL;
}

#if PARAM_NAME || PARAM_TEXT
/**
 * @brief Find param descriptor by text field.
 *
 * @param params The target parameters collection.
 * @param field The required text field.
 * @param value The required text value.
 * @return The found parameter descriptor or NULL if not found.
 */
const param_desc_t *param_find_text(const param_coll_t *params, param_flag_t field, const char *value, param_size_t length);
#endif

#if PARAM_NAME
static inline const param_desc_t *param_find(const param_coll_t *params, const char *value, param_size_t length) {
  return param_find_text(params, param_none, value, length);
}
#endif

/**
 * @brief Initialize parameter's values
 *
 * @param params The target parameters collection.
 */
void param_init(const param_coll_t *params);

#endif/*__PARAM_H__*/

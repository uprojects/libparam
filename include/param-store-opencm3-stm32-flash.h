#include <libopencm3/stm32/flash.h>

extern const unsigned _param_start;
extern const unsigned _param_end;

static const addr_t store_start = (addr_t)&_param_start;
static const addr_t store_end = (addr_t)&_param_end;

#define store_align 2
#define store_sector (1 << 10)

static inline void store_lock(void) {
  flash_lock();
}

static inline void store_unlock(void) {
  flash_unlock();
}

static inline void store_erase(addr_t addr) {
  flash_erase_page(addr);
}

static inline void store_read(addr_t addr, param_cell_t *ptr, param_size_t len) {
  memcpy(ptr, (const void*)addr, len);
}

static inline void store_write(addr_t addr, const param_cell_t *ptr, param_size_t len) {
  addr_t end = addr + (len & ~0x3);
  
  for (; addr < end; ) {
    flash_program_word(addr, *(const uint32_t*)ptr);
    addr += 4;
    ptr = (const param_cell_t *)(((const uint32_t*)ptr) + 1);
  }
  
  end = addr + (len & 0x3);
  
  for (; addr < end; ) {
    flash_program_half_word(addr, *(const uint16_t*)ptr);
    addr += 2;
    ptr = (const param_cell_t *)(((const uint16_t*)ptr) + 1);
  }
}

#ifndef __PARAM_ESTR_H__
#define __PARAM_ESTR_H__ "param-estr.h"

/**
 * @brief Get type of parameter to string.
 *
 * @param param The target parameter descriptor.
 * @param str The string object to output to.
 * @return The operation status.
 *
 * If result equalts to ptr, then type string cannot be placed into buffer.
 */
int param_getestr_type(const param_desc_t *param, estr_t *str);

#if PARAM_CONS
/**
 * @brief Get value of parameter constraint field to string.
 *
 * @param param The target parameter descriptor.
 * @param field The target parameter constraint field.
 * @param ptr The string object to output to.
 * @return The operation status.
 */
int param_getestr_cons(const param_desc_t *param, param_flag_t field, estr_t *str);
#endif/*PARAM_CONS*/

/**
 * @brief Get value of parameter to string.
 *
 * @param param The target parameter descriptor.
 * @param str The string object to output to.
 * @return The operation status.
 */
int param_getestr(const param_desc_t *param, estr_t *str);

#endif/*__PARAM_ESTR_H__*/

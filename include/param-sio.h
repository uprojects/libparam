#ifndef __PARAM_SIO_H__
#define __PARAM_SIO_H__ "param-sio.h"

/**
 * @brief String serialization util.
 */
int put_quoted_str(char **ptr, char *end, const char *str);

/**
 * @brief Get type of parameter to string.
 *
 * @param param The target parameter descriptor.
 * @param ptr The pointer to pointer to start of destination string buffer.
 * @param end The pointer to end of destination string buffer.
 * @return The operation status.
 *
 * If result equalts to ptr, then type string cannot be placed into buffer.
 */
int param_getstr_type(const param_desc_t *param, char **ptr, char *end);

/**
 * @brief Get size of parameter to string.
 *
 * @param param The target parameter descriptor.
 * @param ptr The pointer to pointer to start of destination string buffer.
 * @param end The pointer to end of destination string buffer.
 * @return The operation status.
 *
 * If result equalts to ptr, then size string cannot be placed into buffer.
 */
int param_getstr_size(const param_desc_t *param, char **ptr, char *end);

#if PARAM_CONS
/**
 * @brief Get value of parameter constraint field to string.
 *
 * @param param The target parameter descriptor.
 * @param field The target parameter constraint field.
 * @param ptr The pointer to pointer to start of destination string buffer.
 * @param end The pointer to end of destination string buffer.
 * @return The operation status.
 */
int param_getstr_cons(const param_desc_t *param, param_flag_t field, char **ptr, char *end);
#endif/*PARAM_CONS*/

/**
 * @brief Get value of parameter to string.
 *
 * @param param The target parameter descriptor.
 * @param ptr The pointer to pointer to start of destination string buffer.
 * @param end The pointer to end of destination string buffer.
 * @return The operation status.
 */
int param_getstr(const param_desc_t *param, char **ptr, char *end);

/**
 * @brief Set value of parameter from string.
 *
 * @param param The target parameter descriptor.
 * @param ptr The pointer to pointer to start of source string buffer.
 * @param end The pointer to end source string buffer.
 * @return The operation status.
 */
int param_setstr(const param_desc_t *param, const char **ptr, const char *end);

#endif/*__PARAM_SIO_H__*/

#ifndef __PARAM_JSON_H__
#define __PARAM_JSON_H__ "param-json.h"

int param_json_handle(const param_coll_t *params,
                      const char *iptr, const char *iend,
                      char *optr, char *oend);

#endif/*__PARAM_JSON_H__*/

#define PARAM_BOOL 1
#define PARAM_SINT 1
#define PARAM_UINT 1
#define PARAM_REAL 1
#define PARAM_CSTR 1
#define PARAM_HBIN 1
#define PARAM_ENUM 1

#define PARAM_8BIT 1
#define PARAM_16BIT 1
#define PARAM_32BIT 1

#ifdef TEST_DOUBLE
#define PARAM_64BIT 1
#endif

#define PARAM_FIX 1
#define PARAM_DEF 1
#define PARAM_MIN 1
#define PARAM_MAX 1
#define PARAM_STP 1

#define PARAM_NAME 1
#define PARAM_UNIT 1
#define PARAM_INFO 1
#define PARAM_HINT 1

#define PARAM_VIRT 1

#include "../src/param.c"
#include "../src/param-iface.c"

#include "test.h"

/* Parameter's definions */

#define simple_def { "simple", param_none,      \
      0, NULL, NULL, NULL }

static const char *const text_const_text[] = {
  "The textual constant parameter."
};

static const char *const text_const_cons[] = {
  "Some predefined textual constant value."
};

#define text_const_def { "text_const",            \
      param_def | param_info,                     \
      0, NULL, text_const_cons, text_const_text }

static const char *const ro_uint8_text[] = {
  "The 8-bit unsigned int parameter, which cannot be modified from outside."
};

static uint8_t ro_uint8_data = 84;

#define ro_uint8_def { "ro_uint8",             \
      param_getable | param_uint | param_info, \
      sizeof(ro_uint8_data), &ro_uint8_data,   \
      NULL, ro_uint8_text }

static const char *const rw_text_text[] = {
  "The getable and setable textual parameter."
};

static const char *const rw_text_cons[] = {
  "Default text value."
};

static char *rw_text_data[32];

#define rw_text_def { "rw_text",          \
      param_def | param_info,             \
      sizeof(rw_text_data), rw_text_data, \
      rw_text_cons, rw_text_text }

enum {
  first = 0,
  second,
  third,
};

static const char *const rw_enum_text[] = {
  "The getable and setable enum parameter.",
  "first", "The first available value.",
  "second", "The second available value.",
  "third", "The third available value.",
};

static const uint8_t rw_enum_cons[] = {
  second, /* use by default */
  first, /* minimum */
  third, /* maximum */
};

static uint8_t rw_enum_data;

#define rw_enum_def { "rw_enum",           \
      param_getable | param_setable |      \
      param_enum | param_info |            \
      param_def | param_min | param_max,   \
      sizeof(rw_enum_data), &rw_enum_data, \
      rw_enum_cons, rw_enum_text }

static const char *const rw_int32_text[] = {
  "Steps",
  "The integer value parameter with units and constraints."
};

static const int32_t rw_int32_cons[] = {
  0, /* default */
  -31001, /* min */
  65000, /* max */
  10,  /* step */
};

static int32_t rw_int32_data;

#define rw_int32_def { "rw_int32",                   \
      param_getable | param_setable |                \
      param_real | param_unit | param_info |         \
      param_def | param_min | param_max | param_stp, \
      sizeof(rw_int32_data), &rw_int32_data,         \
      rw_int32_cons, rw_int32_text }

static const char *const rw_float_text[] = {
  "Celsius",
  "The floating point value parameter with units and constraints."
};

static const float rw_float_cons[] = {
  11.1, /* default */
  -4.5, /* min */
  41.8, /* max */
  0.1,  /* step */
};

static float rw_float_data;

#define rw_float_def { "rw_float",                   \
      param_getable | param_setable |                \
      param_real | param_unit | param_info |         \
      param_def | param_min | param_max | param_stp, \
      sizeof(rw_float_data), &rw_float_data,         \
      rw_float_cons, rw_float_text }

PARAM_COLL(params,
           simple_def,
           text_const_def,
           ro_uint8_def,
           rw_text_def,
           rw_enum_def,
           rw_int32_def,
           rw_float_def);

static uint8_t buffer[1024];

static void exchange(param_size_t *len) {
  param_iface_handle(&params, buffer, *len, buffer, len);
}

int main() {
  param_init(&params);

  param_size_t len;
  exchange(&len);
  
  return 0;
}

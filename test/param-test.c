#include "param.h"
#include "param-sio.h"
#include "param-json.h"

#define VERSION 0,0,1,0

#include "param-test.h"

typedef enum {
  ctl_off,
  ctl_man,
  ctl_auto,
} ctl_mode_t;

static struct {
  ctl_mode_t mode;
} ctl;

static float real_val;

static int real_get(const param_desc_t *param, float *data) {
  (void)param;
  *data = real_val;
  return param_success;
}

static int real_set(const param_desc_t *param, const float *data) {
  (void)param;
  real_val = *data;
  return param_success;
}

typedef uint8_t aes128_key_t[16];

aes128_key_t aes_key;

static int aes_key_get(const param_desc_t *param, aes128_key_t *data) {
  (void)param;
  memcpy(data, &aes_key, sizeof(aes_key));
  return param_success;
}

static int aes_key_set(const param_desc_t *param, const aes128_key_t *data) {
  (void)param;
  PARAM_ROM_READ(&aes_key, data, sizeof(aes_key));
  return param_success;
}

#define __params_c__
#include "param-test.h"

#include <string.h>
#include "test.h"

void test_io(const param_coll_t *params,
             const char *des,
             const char *req,
             const char *res) {
  static char buf[1024];
  static char *ptr, *end;
  
  info("%s", des);
  strcpy(buf, req);
  ptr = buf;
  end = buf + sizeof(buf);
  int len = param_json_handle(params,
                              ptr, ptr + strlen(buf),
                              ptr, end);
  buf[len] = '\0';
  if (0 != strcmp(buf, res)) {
    fail("Invalid response to request: '%s'! "
         "Expected: '%s' Actual: '%s'", req, res, buf);
  } else {
    pass();
  }
}

#define unwr(...) #__VA_ARGS__
#define test(des, req, res) test_io(&params, #des, unwr req, unwr res)

int main(void) {
  param_init(&params);

  test(incomplete, (), ());
  test(incomplete2, ({), ());
  test(incomplete3, ({"info"), ());
  test(incomplete4, ({"info":"param"), ());
  test(incomplete5, ({"gets":["param1"), ());
  test(invalid, ({}), ({"error":"Invalid json request"}));
  test(invalid2, ({"data":true}), ({"error":"Invalid json request"}));
  test(invalid3, ({"info":1}), ({"error":"Invalid json request"}));
  
  test(list, ({"info":null}), (["group","version","u8_num","some_str","real_num","ctl_mode","aes_key","ip_addr","mac_addr"]));
  
  test(info: group, ({"info":"group"}), ({"info":"Parameters"}));
  test(info: version, ({"info":"version"}), ({"type":"ipv4","size":4,"def":"0.0.1.0","info":"Version number"}));
  test(info: u8_num, ({"info":"u8_num"}), ({"type":"uint","size":1,"flag":["getable"],"def":11,"unit":"pcs","info":"The readonly 8-bit unsigned integer with unit"}));
  test(info: some_str, ({"info":"some_str"}), ({"type":"cstr","size":32,"flag":["getable","setable","persist"]}));
  test(info: real_num, ({"info":"real_num"}), ({"type":"real","size":4,"flag":["getable","setable","persist"],"def":0,"min":-1,"max":1,"info":"The floating point number with getter and setter"}));
  test(info: ctl_mode, ({"info":"ctl_mode"}), ({"type":"enum","size":4,"flag":["getable","setable","persist"],"def":"auto","info":"Control mode","enum":{"off":"Disabled","auto":"Automatic","man":"Manual"}}));
  test(info: aes_key, ({"info":"aes_key"}), ({"type":"hbin","size":16,"flag":["getable","setable","persist"],"def":"e3e53ec196e03f28e3221730dde2b43c"}));
  test(info: ip_addr, ({"info":"ip_addr"}), ({"type":"ipv4","size":4,"flag":["getable","setable","persist"],"def":"192.168.1.1"}));
  test(info: mac_addr, ({"info":"mac_addr"}), ({"type":"mac","size":6,"flag":["getable","setable"],"def":"12:34:56:ab:cd:ef","info":"ARP mac address of interface."}));

  /* none */
  test(gets: group=null, ({"gets":["group"]}), ([null]));
  test(gets: group=null version=null, ({"gets":["group","version"]}), ([null,null]));

  /* nums */
  test(gets: u8_num=11 real_num=0, ({"gets":["u8_num","real_num"]}), ([11,0]));
  test(gets: real_num=0 u8_num=11, ({"gets":["real_num","u8_num"]}), ([0,11]));
  test(sets: u8_num=10 real_num=0.5, ({"sets":{"u8_num":10,"real_num":0.5}}), ({"u8_num":null,"real_num":true}));
  test(gets: u8_num=11 real_num=0.5, ({"gets":["u8_num","real_num"]}), ([11,0.5]));
  u8_num = 1;
  test(gets: real_num=0.5 u8_num=1, ({"gets":["real_num","u8_num"]}), ([0.5,1]));
  test(sets: u8_num=-1 real_num=5, ({"sets":{"u8_num":-1,"real_num":5}}), ({"u8_num":null,"real_num":true}));
  test(gets: real_num=1 u8_num=1, ({"gets":["real_num","u8_num"]}), ([1,1]));
  
  /* cstr */
  test(gets: some_str="", ({"gets":["some_str"]}), ([""]));
  test(sets: some_str="Hello world!", ({"sets":{"some_str":"Hello world!"}}), ({"some_str":true}));
  test(gets: some_str="Hello world!", ({"gets":["some_str"]}), (["Hello world!"]));
  test(sets: some_str="Чертовски длинная строка", ({"sets":{"some_str":"Чертовски длинная строка"}}), ({"some_str":false}));
  test(gets: some_str!="Чертовски длинная строка", ({"gets":["some_str"]}), (["Hello world!"]));
  test(sets: some_str=Короткая строка, ({"sets":{"some_str":"Короткая строка"}}), ({"some_str":true}));
  test(gets: some_str=Короткая строка, ({"gets":["some_str"]}), (["Короткая строка"]));
  
  /* enum */
  test(gets: ctl_mode=auto, ({"gets":["ctl_mode"]}), (["auto"]));
  test(sets: ctl_mode=on, ({"sets":{"ctl_mode":"on"}}), ({"ctl_mode":false}));
  test(gets: ctl_mode=auto, ({"gets":["ctl_mode"]}), (["auto"]));
  test(sets: ctl_mode=off, ({"sets":{"ctl_mode":"off"}}), ({"ctl_mode":true}));
  test(gets: ctl_mode=auto, ({"gets":["ctl_mode"]}), (["off"]));
  test(sets: ctl_mode=man, ({"sets":{"ctl_mode":"man"}}), ({"ctl_mode":true}));
  test(gets: ctl_mode=man, ({"gets":["ctl_mode"]}), (["man"]));
  test(sets: ctl_mode=auto, ({"sets":{"ctl_mode":"auto"}}), ({"ctl_mode":true}));
  test(gets: ctl_mode=auto, ({"gets":["ctl_mode"]}), (["auto"]));

  /* hex bin */
  test(gets: aes_key=e3e53ec196e03f28e3221730dde2b43c, ({"gets":["aes_key"]}), (["e3e53ec196e03f28e3221730dde2b43c"]));
  test(sets: aes_key=e3e53ec196e03f28e3221730dde2b4ce, ({"sets":{"aes_key":"e3e53ec196e03f28e3221730dde2b4ce"}}), ({"aes_key":true}));
  test(gets: aes_key=e3e53ec196e03f28e3221730dde2b4ce, ({"gets":["aes_key"]}), (["e3e53ec196e03f28e3221730dde2b4ce"]));

  /* spec */
  test(gets: ip_addr=192.168.1.1 mac_addr=12:34:56:AB:CD:EF, ({"gets":["ip_addr","mac_addr"]}), (["192.168.1.1","12:34:56:ab:cd:ef"]));
  test(sets: ip_addr=192.168.254.1 mac_addr=12:34:56:AB:CD:GH, ({"sets":{"ip_addr":"192.168.254.1","mac_addr":"12:34:56:AB:CD:GH"}}), ({"ip_addr":true,"mac_addr":false}));
  test(gets: ip_addr=192.168.254.1 mac_addr=12:34:56:AB:CD:EF, ({"gets":["ip_addr","mac_addr"]}), (["192.168.254.1","12:34:56:ab:cd:ef"]));
  test(sets: ip_addr=192.168.259.1 mac_addr=FE:DC:BA:65:43:21, ({"sets":{"ip_addr":"192.168.259.1","mac_addr":"FE:DC:BA:65:43:21"}}), ({"ip_addr":false,"mac_addr":true}));
  test(gets: ip_addr=192.168.254.1 mac_addr=FE:DC:BA:65:43:21, ({"gets":["ip_addr","mac_addr"]}), (["192.168.254.1","fe:dc:ba:65:43:21"]));

  /* reset data to simulate power off */
  strcpy(some_str, "Hello");
  real_num = 0;
  ctl.mode = ctl_off;
  memset(aes_key, 0, sizeof(aes_key));
  *(uint32_t*)ip_addr = 0x00112233;

  /* restore data */
  param_init(&params);

  test(gets after power off: some_str real_num ctl_mode aes_key,
       ({"gets":["some_str","real_num","ctl_mode","aes_key"]}),
       (["Короткая строка",1,"auto","e3e53ec196e03f28e3221730dde2b4ce"]));
  
  return 0;
}

#include "../src/param.c"
#include "../src/param-sio.c"
#include "../src/param-json.c"
#include "../../json_parser/src/tokenizer.c"
#include "../src/param-store.c"

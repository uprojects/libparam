#define PARAM_BOOL 1
#define PARAM_SINT 1
#define PARAM_UINT 1
#define PARAM_SFIX 1
#define PARAM_UFIX 1
#define PARAM_REAL 1
#define PARAM_CSTR 1
#define PARAM_HBIN 1
#define PARAM_8BIT 1
#define PARAM_16BIT 1
#define PARAM_32BIT 1

#ifdef TEST_DOUBLE
#define PARAM_64BIT 1
#endif

#include "../src/param.c"
#include "../src/param-sio.c"

#include "test.h"

#define abs(a) (((a) > 0) ? (a) : (- (a)))

typedef uint_t tag_type_u;
typedef sint_t tag_type_i;
typedef real_t tag_type_f;
typedef uint_t tag_type_uf;
typedef sint_t tag_type_if;

#if PARAM_64BIT
#define tag_fmt_u "%lu"
#define tag_fmt_i "%li"
#define tag_fmt_f "%g"
#define tag_fmt_if "%g"
#define tag_fmt_uf "%g"
#else
#define tag_fmt_u "%u"
#define tag_fmt_i "%i"
#define tag_fmt_f "%f"
#define tag_fmt_if "%f"
#define tag_fmt_uf "%f"
#endif

#define test_sto_(tag, val, str, len, res, to_val, from_val, ...) \
  {                                                               \
    info("sto" #tag " " str);                                     \
    const char *ptr = str;                                        \
    tag_type_##tag exv = to_val(val), acv;                        \
    int r = sto##tag(&ptr, str + strlen(str), &acv,               \
                     ##__VA_ARGS__);                              \
    if (r != param_##res) {                                       \
      fail("Invalid return value! Expected: %s Actual: %s",       \
           param_res_str(param_##res), param_res_str(r));         \
    } else if (ptr - str != len) {                                \
      fail("Invalid consumed length! Expected: %u Actual: %u",    \
           len, (int)(ptr - str));                                \
    } else if (abs(acv - exv) > 1e-6) {                           \
      fail("Invalid result value! Expected: " tag_fmt_##tag       \
           " Actual: " tag_fmt_##tag,                             \
           from_val(exv), from_val(acv));                         \
    } else {                                                      \
      pass();                                                     \
    }                                                             \
  }

#define id(x) (x)

#define test_stox(tag, val, str, len, res)      \
  test_sto_(tag, val, str, len, res, id, id)

#define test_xtos(tag, val, str)                                  \
  {                                                               \
    info(#tag "tos " str);                                        \
    char buf[64];                                                 \
    char *ptr = buf;                                              \
    int r = tag##tos(&ptr, buf + sizeof(buf), val);               \
    *ptr = '\0';                                                  \
    if (r != param_success) {                                     \
      fail("Invalid return value! Expected: %s Actual: %s",       \
           param_res_str(param_success), param_res_str(r));       \
    } else if (strcmp(str, buf) != 0) {                           \
      fail("Invalid result value! Expected: '%s' Actual: '%s'",   \
           str, buf);                                             \
    } else {                                                      \
      pass();                                                     \
    }                                                             \
  }

#define test_ftos(exp, val, str)                                  \
  {                                                               \
    info("ftos " str);                                            \
    char buf[64];                                                 \
    char *ptr = buf;                                              \
    int r = ftos(&ptr, buf + sizeof(buf), val, exp);              \
    *ptr = '\0';                                                  \
    if (r != param_success) {                                     \
      fail("Invalid return value! Expected: %s Actual: %s",       \
           param_res_str(param_success), param_res_str(r));       \
    } else if (strcmp(str, buf) != 0) {                           \
      fail("Invalid result value! Expected: '%s' Actual: '%s'",   \
           str, buf);                                             \
    } else {                                                      \
      pass();                                                     \
    }                                                             \
  }

#define to_q(q, x) ((x) * (1 << (q)))
#define from_q(q, x) ((double)(x) / (1 << (q)))

#define to_q16(x) to_q(16, x)
#define from_q16(x) from_q(16, x)

#define test_stoq(q, val, str, len, res)      \
  test_sto_(if, val, str, len, res, to_q##q, from_q##q, q)

#define test_qtos(q, val, str)                                    \
  {                                                               \
    info("iftos " str);                                           \
    char buf[64];                                                 \
    char *ptr = buf;                                              \
    int r = iftos(&ptr, buf + sizeof(buf), to_q##q(val), q);      \
    *ptr = '\0';                                                  \
    if (r != param_success) {                                     \
      fail("Invalid return value! Expected: %s Actual: %s",       \
           param_res_str(param_success), param_res_str(r));       \
    } else if (strcmp(str, buf) != 0) {                           \
      fail("Invalid result value! Expected: '%s' Actual: '%s'",   \
           str, buf);                                             \
    } else {                                                      \
      pass();                                                     \
    }                                                             \
  }

int main() {
  printf("min_uint: " tag_fmt_u "\nmax_uint: " tag_fmt_u "\n", min_uint, max_uint);
  printf("min_sint: " tag_fmt_i "\nmax_sint: " tag_fmt_i "\n", min_sint, max_sint);
  
  test_stox(u, 0, "0", 1, success);
  test_stox(u, 1, "1", 1, success);
  test_stox(u, 2015, "2015", 4, success);
  test_stox(u, 123, "123, ", 3, invalid);
  test_stox(u, 123, "123+123", 3, invalid);
  test_stox(u, 0, "-123", 0, invalid);
  test_stox(u, 0, "00000", 5, success);
  test_stox(u, 1, "00001", 5, success);
  
  test_stox(i, 0, "0", 1, success);
  test_stox(i, 1, "1", 1, success);
  test_stox(i, 0, "-0", 2, success);
  test_stox(i, -1, "-1", 2, success);
  test_stox(i, 2015, "2015", 4, success);
  test_stox(i, -2015, "-2015", 5, success);
  test_stox(i, 123, "123, ", 3, invalid);
  test_stox(i, 123, "123+123", 3, invalid);
  test_stox(i, -123, "-123", 4, success);
  test_stox(i, 0, "-00000", 6, success);
  test_stox(i, -1, "-00001", 6, success);
  
  test_stox(f, 0, "0", 1, success);
  test_stox(f, 1, "1", 1, success);
  test_stox(f, 0, "-0", 2, success);
  test_stox(f, -1, "-1", 2, success);
  test_stox(f, 1234, "1234", 4, success);
  test_stox(f, -1234, "-1234", 5, success);
  test_stox(f, 1234, "1234 , ", 4, invalid);
  test_stox(f, 0.456, "0.456 -", 5, invalid);
  test_stox(f, -0.456, "-0.456+ 1", 6, invalid);
  test_stox(f, 1.001, "1.001", 5, success);
  test_stox(f, -1.001, "-1.001", 6, success);
  test_stox(f, 0.00001, "0.00001", 7, success);
  test_stox(f, 1000.00001, "1000.00001", 10, success);
  test_stox(f, -1000.00001, "-1000.00001", 11, success);
  test_stox(f, 123.456, "123.456 , ", 7, invalid);
  test_stox(f, -123.456, "-123.456:", 8, invalid);

  test_stoq(16, 0, "0", 1, success);
  test_stoq(16, 1, "1", 1, success);
  test_stoq(16, 0, "-0", 2, success);
  test_stoq(16, -1, "-1", 2, success);
  test_stoq(16, 1234, "1234", 4, success);
  test_stoq(16, -1234, "-1234", 5, success);
  test_stoq(16, 1234, "1234 , ", 4, invalid);
  test_stoq(16, 0.456, "0.456 -", 5, invalid);
  test_stoq(16, -0.456, "-0.456+ 1", 6, invalid);
  test_stoq(16, 1.001, "1.001", 5, success);
  test_stoq(16, -1.001, "-1.001", 6, success);
  test_stoq(16, 0.00001, "0.00001", 7, success);
  test_stoq(16, 1000.00001, "1000.00001", 10, success);
  test_stoq(16, -1000.00001, "-1000.00001", 11, success);
  test_stoq(16, 123.456, "123.456 , ", 7, invalid);
  test_stoq(16, -123.456, "-123.456:", 8, invalid);

  test_xtos(u, 0, "0");
  test_xtos(u, 1, "1");
  test_xtos(u, 2015, "2015");
  
  test_xtos(i, 0, "0");
  test_xtos(i, 1, "1");
  test_xtos(i, -1, "-1");
  test_xtos(i, 2015, "2015");
  test_xtos(i, -2015, "-2015");
  test_xtos(i, -123, "-123");

  test_ftos(10, 0, "0");
  test_ftos(10, 1, "1");
  test_ftos(10, -1, "-1");
  test_ftos(10, 1234, "1234");
  test_ftos(10, -1234, "-1234");
  test_ftos(7, 1.001, "1.001");
  test_ftos(7, -1.001, "-1.001");
  test_ftos(10, 0.00001, "0.00001");
  test_ftos(4, 1000.0001, "1000.0001");
  test_ftos(4, -1000.0001, "-1000.0001");

  test_qtos(16, 0, "0");
  test_qtos(16, 1, "1");
  test_qtos(16, -1, "-1");
  test_qtos(16, 1234, "1234");
  test_qtos(16, -1234, "-1234");
  test_qtos(16, 1.001, "1.0009918212890625");
  test_qtos(16, -1.001, "-1.0009918212890625");
  test_qtos(16, 0.00001, "0");
  test_qtos(16, 1000.0001, "1000.000091552734375");
  test_qtos(16, -1000.0001, "-1000.000091552734375");

  return 0;
}

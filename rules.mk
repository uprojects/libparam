# libparam (parameter-based control)
libparam.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

libparam.store ?= # opencm3-stm32-flash | esp8266-spi-flash
libparam.parts ?= # sio estr json iface

libparam.types ?= enum uint sint ufix sfix real cstr hbin ipv4 mac
libparam.texts ?= name unit info item hint
libparam.const ?= def min max step
libparam.flags ?= virtual persist fix 8bit 16bit 32bit # double

libparam.FLAGS = $(foreach g,types texts const flags,$(libparam.$(g)))

libparam.DEFMAP := \
enum:ENUM uint:UINT sint:SINT ufix:UFIX sfix:SFIX real:REAL cstr:CSTR hbin:HBIN ipv4:IPV4 mac:MAC \
fix:FIX def:DEF min:MIN max:MAX step:STP virtual:VIRT persist:PERS \
8bit:8BIT 16bit:16BIT 32bit:32BIT 64bit:64BIT \
name:NAME unit:UNIT info:INFO item:ITEM hint:HINT

TARGET.LIBS += libparam
libparam.SRCS += $(patsubst %,$(libparam.BASEPATH)%.c,\
src/param \
$(if $(filter sio json,$(libparam.parts)),src/param-sio) \
$(if $(filter json,$(libparam.parts)),src/param-json) \
$(if $(filter estr,$(libparam.parts)),src/param-estr) \
$(if $(libparam.store),src/param-store))
libparam.CDEFS += $(foreach x,$(filter $(addsuffix :%,$(libparam.FLAGS)),$(libparam.DEFMAP)),PARAM_$(lastword $(subst :, ,$(x)))=1)
libparam.CDEFS += $(if $(libparam.store),PARAM_STORE=$(libparam.store))
libparam.CDIRS += $(libparam.BASEPATH)/include

include $(libparam.BASEPATH)define.mk

param.GENDIR ?= gen/param
param.GEN_H = $(1).pg.h
param.GEN_P = $(param.GENDIR)/$(call param.GEN_H,$(1))
param.CDIRS := $(param.GENDIR)

# <input> <output> <namespace> <language> <depends...>
param-make-rules = $(call param-make,$(word 2,$(1)),$(call param.GEN_P,$(word 1,$(1))),$(word 1,$(1)),$(word 3,$(1)),$(2))
param-file-def = PARAMS_$(word 1,$(1))_H='"$(call param.GEN_H,$(word 1,$(1)))"'

$(param.GENDIR)/:
	$(Q)mkdir -p $@

# <namespace>:<input>:<language>
define PAR_RULES
ifneq (,$$($(1).PARS))
$(1).INHERIT += param
$(1).CDEFS += $$(foreach c,$$($(1).PARS),$$(call param-file-def,$$(subst :, ,$$(c))))
$$(foreach c,$$($(1).PARS),$$(call param-make-rules,$$(subst :, ,$$(c)),$$($(1).SRCS)))
endif
endef

param.dsl.log ?= debug~

param-log = $(if $(filter $(1),$(param.dsl.log)),$(info $(2)$(3)$(4)$(5)))

# parameter fields
param.base.fields := type size
param.bind.fields := cell get set
param.cons.fields := def
param.text.fields := unit info item hint
param.all.fields := $(foreach g,base bind cons text,$(param.$(g).fields))

param.types := none enum uint sint ufix sfix real cstr hbin mac ipv4
param.flags := getable setable persist

# parameter definition comments
param.def.cmt := parameter definition
param.des.cmt := parameter descriptor

param.base.cmt := basic fields
param.bind.cmt := binding fields
param.cons.cmt := constraint fields
param.text.cmt := textual fields

param.type.cmt := type
param.name.cmt := name
param.flag.cmt := flags
param.size.cmt := size of value

param.data.cmt := parameter data
param.cell.cmt := pointer to data cell
param.get.cmt := getter function
param.set.cmt := setter function

param.def.cmt := default value
param.min.cmt := minimum
param.max.cmt := maximum
param.stp.cmt := step

param.info.cmt := short description
param.unit.cmt := measurement units
param.item.cmt := item for menu
param.hint.cmt := hint for menu

param.persist.cmt := persistent
param.virtual.cmt := virtual
param.getable.cmt := readable
param.setable.cmt := writable

param.typedef.cmt := type definition

param.enum-cnt.cmt := num of enum options
param.enum-val.cmt := enum option value
param.enum-name.cmt := enum option name

param.fix-fract.cmt := fixed point fraction

param.def.opt := DEF
param.min.opt := MIN
param.max.opt := MAX
param.stp.opt := STP

param.info.opt := INFO
param.unit.opt := UNIT
param.item.opt := ITEM
param.hint.opt := HINT

param.persist.opt := PERS
param.virtual.opt := VIRT

param.cons.def.init- = static const $(if $(3),typeof($(3)),$(call param.$(2).type,$(1))) $(1)_cons[] PARAM_ROM_ATTR = {
param.cons.def.init = $(call param.cons.def.init-,$(1),$(2),$(call param-bind,$(1)))
param.cons.def.done = };
param.cons.def.data = $(or $(call param.$(2).cons.def.data,$(3),$(1)),$(3))
# <param> <cons> <type>
param.cons.def-- = $(if $(or $(2),$(filter enum ufix sfix,$(3))),\
$(file >>$@,\#if PARAM_CONS)\
$(file >>$@,$(or $(call param.$(3).cons.def.init,$(1),$(3)),$(call param.cons.def.init,$(1),$(3)))$(call param-cmt,cons,$(1)))\
$(foreach c,$(filter $(2),$(param.$(3).cons.fields)),\
$(file >>$@,\#if PARAM_$(param.$(c).opt))\
$(file >>$@,  $(call param.cons.def.data,$(1),$(3),$(call param-get,$(1),$(c)))$(call param-sep,$(c),$(4))\
$(call param-cmt,$(c),$(1),$(c)))\
$(file >>$@,\#endif /* PARAM_$(param.$(c).opt) */))\
$(call param.$(3).cons.def.extra,$(1),$(2))\
$(file >>$@,$(or $(call param.$(3).cons.def.done,$(1)),$(call param.cons.def.done,$(1))))\
$(file >>$@,\#endif /* PARAM_CONS */)\
$(file >>$@,))
param.cons.def- = $(call param.cons.def--,$(1),$(2),$(3),$(strip $(2) $(call param.$(3).cons.def.extra-list,$(1),$(2))))
# <param>
param.cons.def = $(call param.cons.def-,$(1),$(call param-cons,$(1)),$(call param-type,$(1)))

param.text.def- = $(if $(or $(2),$(filter enum,$(call param-type,$(1)))),\
$(file >>$@,\#if PARAM_TEXT)\
$(file >>$@,static const char *const $(1)_text[] PARAM_ROM_ATTR = {$(call param-cmt,text,$(1)))\
$(foreach t,$(filter $(2),$(param.text.fields)),\
$(file >>$@,\#if PARAM_$(param.$(t).opt))\
$(file >>$@,  "$(call param-get,$(1),$(t))",$(call param-cmt,$(t),$(1),$(t)))\
$(file >>$@,\#endif /* PARAM_$(param.$(t).opt) */))\
$(call param.$(call param-type,$(1)).text.def.extra,$(1),$(2))\
$(file >>$@,};)\
$(file >>$@,\#endif /* PARAM_TEXT */)\
$(file >>$@,))
param.text.def = $(call param.text.def-,$(1),$(call param-text,$(1)))

param.data.def- = $(if $(3),$(file >>$@,extern $(call param.$(2).data.def,$(1),$(2),$(or $(4),$(3)));))
param.data.def = $(call param.data.def-,$(1),$(call param-type,$(1)),$(call param-data,$(1)),$(call param-bind,$(1)))
param.data.imp- = $(if $(3),\
$(file >>$@,$(call param.$(2).data.def,$(1),$(2),$(3));$(call param-cmt,data,$(1)))\
$(file >>$@,))
param.data.imp = $(call param.data.imp-,$(1),$(call param-type,$(1)),$(call param-data,$(1)))
param.data.cell- = $(if $(5),(param_vmt_t*)&$(1)_vmt,$(if $(or $(3),$(4)),$(call param.$(2).data.ptr,$(1),$(2),$(or $(4),$(3))),NULL))
param.data.cell = $(call param.data.cell-,$(1),$(call param-type,$(1)),$(call param-data,$(1)),$(call param-bind,$(1)),$(call param-virt,$(1)))
param.data.size- = $(if $(or $(3),$(4)),sizeof($(or $(4),$(3))),$(call param.$(2).size,$(1)))
param.data.size = $(call param.data.size-,$(1),$(call param-type,$(1)),$(call param-data,$(1)),$(call param-bind,$(1)),$(call param-size,$(1)))

param.vmt.def- = \
$(file >>$@,\#if PARAM_VIRT)\
$(file >>$@,static const param_vmt_t $(1)_vmt = {$(call param-cmt,virtual,$(1)))\
$(file >>$@,  (param_get_t*)$(strip $(2)),$(call param-cmt,get,$(1)))\
$(file >>$@,  (param_set_t*)$(strip $(3)) $(call param-cmt,set,$(1)))\
$(file >>$@,};)\
$(file >>$@,\#endif /* PARAM_VIRT */)\
$(file >>$@,)
param.vmt.def = $(if $(call param-virt,$(1)),$(call param.vmt.def-,$(1),\
$(or $(call param-get,$(1),get,NULL),$(1)_get),\
$(or $(call param-get,$(1),set,NULL),$(1)_set)))

param-enum-cons- = $(file >>$@,  $(words $(2)),$(call param-cmt,enum-cnt,$(1)))\
$(foreach o,$(2),$(file >>$@,  $(param.node.value.$(1).$(o).val),$(call param-cmt,enum-val,$(1))))
param-enum-cons = $(call param-enum-cons-,$(1),$(param.node.value.$(1)))

param-enum-text- = $(foreach o,$(3),\
$(file >>$@,\#if PARAM_NAME)\
$(file >>$@,  "$(o)",$(call param-cmt,enum-name,$(1)))\
$(file >>$@,\#endif /* PARAM_NAME */)\
$(foreach t,$(filter $(2),$(param.text.fields)),\
$(file >>$@,\#if PARAM_$(param.$(t).opt))\
$(file >>$@,  "$(call param-get,$(o),$(t),,$(1))",$(call param-cmt,$(t),$(1),$(t)))\
$(file >>$@,\#endif /* PARAM_$(param.$(t).opt) */)))
param-enum-text = $(call param-enum-text-,$(1),$(2),$(param.node.value.$(1)))

param.fix-fract.8 = 4
param.fix-fract.16 = 8
param.fix-fract.32 = 16
param.fix-fract.64 = 32
param-fix-fract = $(call param-get,$(1),fract,$(param.fix-fract.$(call param-size,$(1))))
param-fix-cons = $(file >>$@,  $(call param-fix-fract,$(1))$(call param-cmt,fix-fract,$(1)))

# parameter type specific overrides
param.none.size = 0
param.none.cell = NULL

param.atom.type = $(if $(filter $(2),$(call param-size,$(1))),$(3),$(error Invalid size of param $(1)! You can use $(2) bits.))
param.atom.size = sizeof($(call param.$(call param-type,$(1)).type,$(1)))
param.atom.data.def = $(call param.$(2).type,$(1)) $(3)
param.atom.data.ptr = &$(3)
param.num.cons.fields = $(param.cons.fields) min max stp

param.uint.type = $(call param.atom.type,$(1),8 16 32 64,uint$(call param-size,$(1))_t)
param.uint.size = $(call param.atom.size,$(1))
param.uint.data.def = $(call param.atom.data.def,$(1),$(2),$(3))
param.uint.data.ptr = $(call param.atom.data.ptr,$(1),$(2),$(3))
param.uint.cons.fields = $(param.num.cons.fields)

param.sint.type = $(call param.atom.type,$(1),8 16 32 64,int$(call param-size,$(1))_t)
param.sint.size = $(call param.atom.size,$(1))
param.sint.data.def = $(call param.atom.data.def,$(1),$(2),$(3))
param.sint.data.ptr = $(call param.atom.data.ptr,$(1),$(2),$(3))
param.sint.cons.fields = $(param.num.cons.fields)

param.ufix.type.def = $(file >>$@,\#ifndef PARAM_FIX_VAL)\
$(file >>$@,\#define PARAM_FIX_VAL(x, f) ((x)*(1<<(f))))\
$(file >>$@,\#endif /* PARAM_FIX_VAL */)
param.ufix.type = $(call param.atom.type,$(1),8 16 32 64,uint$(call param-size,$(1))_t)
param.ufix.size = $(call param.atom.size,$(1))
param.ufix.data.def = $(call param.atom.data.def,$(1),$(2),$(3))
param.ufix.data.ptr = $(call param.atom.data.ptr,$(1),$(2),$(3))
param.ufix.cons.fields = $(param.num.cons.fields)
param.ufix.cons.def.extra = $(call param-fix-cons,$(1))
param.ufix.cons.def.extra-list = .
param.ufix.cons.def.data = PARAM_FIX_VAL($(1), $(call param-fix-fract,$(2)))

param.sfix.type = $(call param.atom.type,$(1),8 16 32 64,int$(call param-size,$(1))_t)
param.sfix.size = $(call param.atom.size,$(1))
param.sfix.data.def = $(call param.atom.data.def,$(1),$(2),$(3))
param.sfix.data.ptr = $(call param.atom.data.ptr,$(1),$(2),$(3))
param.sfix.cons.fields = $(param.num.cons.fields)
param.sfix.cons.def.extra = $(call param-fix-cons,$(1))
param.sfix.cons.def.extra-list = .
param.sfix.cons.def.data = PARAM_FIX_VAL($(1), $(call param-fix-fract,$(2)))

param.real.type = $(call param.atom.type,$(1),32 64,$(if $(filter 32,$(call param-size,$(1))),float,double))
param.real.size = $(call param.atom.size,$(1))
param.real.data.def = $(call param.atom.data.def,$(1),$(2),$(3))
param.real.data.ptr = $(call param.atom.data.ptr,$(1),$(2),$(3))
param.real.cons.fields = $(param.num.cons.fields)

param.enum.type = $(call param.uint.type,$(1))
param.enum.size = $(call param.uint.size,$(1))
param.enum.data.def = $(call param.uint.data.def,$(1),$(2),$(3))
param.enum.data.ptr = $(call param.uint.data.ptr,$(1),$(2),$(3))
param.enum.cons.fields = $(param.cons.fields)
param.enum.cons.def.extra = $(call param-enum-cons,$(1))
param.enum.cons.def.extra-list = . $(call param-vals,$(1))
param.enum.text.def.extra = $(call param-enum-text,$(1),$(2))
param.enum.text.def.extra-list = $(call param-vals,$(1))

param.cstr.type = const char *
param.cstr.size = $(call param-size,$(1))
param.cstr.data.def = char $(3)[$(call param-size,$(1))]
param.cstr.data.ptr = $(3)
param.cstr.cons.fields = $(param.cons.fields)
param.cstr.cons.def.init = static const char $(1)_cons[] PARAM_ROM_ATTR =
param.cstr.cons.def.data = $(1)
param.cstr.cons.def.done = ;

param.hbin.type.def = $(foreach s,$(call param-uniq,$(foreach p,$(call param-list,hbin),$(call param-size,$(p)))),\
$(file >>$@,\#ifndef PARAM_HBIN$(s)_TYPEDEF)\
$(file >>$@,\#define PARAM_HBIN$(s)_TYPEDEF)\
$(file >>$@,typedef uint8_t param_hbin$(s)_t[$(s)];$(call param-cmt,typedef,hbin,$(s)))\
$(file >>$@,\#endif /* PARAM_HBIN$(s)_TYPEDEF */))
param.hbin.type = param_hbin$(call param.hbin.size,$(1))_t
#struct { uint8_t data[$(call param.hbin.size,$(1))]; }
param.hbin.size = $(call param-size,$(1))
param.hbin.data.def = $(call param.hbin.type,$(1)) $(3)
param.hbin.data.ptr = $(3).data
param.hbin.cons.fields = $(param.cons.fields)
param.hbin.cons.def.data = { $(subst $(strip) ,$(strip ,) ,$(addprefix 0x,$(strip $(1)))) }

param.ipv4.type.def = $(if $(call param-list,ipv4),\
$(file >>$@,\#ifndef PARAM_IPV4_TYPEDEF)\
$(file >>$@,\#define PARAM_IPV4_TYPEDEF)\
$(file >>$@,typedef uint8_t param_ipv4_t[4];$(call param-cmt,typedef,ipv4))\
$(file >>$@,\#endif /* PARAM_IPV4_TYPEDEF */))
param.ipv4.type = param_ipv4_t
param.ipv4.size = sizeof($(param.ipv4.type))
param.ipv4.data.def = $(param.ipv4.type) $(3)
param.ipv4.data.ptr = $(3)
param.ipv4.cons.fields = $(param.cons.fields)
param.ipv4.cons.def.data = { $(subst .,$(strip ,) ,$(strip $(1))) }

param.mac.type.def = $(if $(call param-list,mac),\
$(file >>$@,\#ifndef PARAM_MAC_TYPEDEF)\
$(file >>$@,\#define PARAM_MAC_TYPEDEF)\
$(file >>$@,typedef uint8_t param_mac_t[6];$(call param-cmt,typedef,mac))\
$(file >>$@,\#endif /* PARAM_MAC_TYPEDEF */))
param.mac.type = param_mac_t
param.mac.size = sizeof($(param.mac.type))
param.mac.data.def = $(param.mac.type) $(3)
param.mac.data.ptr = $(3)
param.mac.cons.fields = $(param.cons.fields)
param.mac.cons.def.data = { $(subst $(strip) ,$(strip ,) ,$(addprefix 0x,$(subst :, ,$(strip $(1))))) }

param.des.def- = \
$(file >>$@,\#if PARAM_NAME)\
$(file >>$@,    "$(1)",$(call param-cmt,name,$(1)))\
$(file >>$@,\#endif /* PARAM_NAME */)\
$(file >>$@,    param_$(call param-type,$(1))$(call param-cmt,type,$(1)))\
$(if $(call param-cons,$(1)),\
$(file >>$@,   $(call param-cmt,cons,$(1))))\
$(foreach c,$(call param-cons,$(1)),\
$(file >>$@,\#if PARAM_$(param.$(c).opt))\
$(file >>$@,    | param_$(c)$(call param-cmt,$(c),$(1)))\
$(file >>$@,\#endif /* PARAM_$(param.$(c).opt) */))\
$(if $(call param-text,$(1)),\
$(file >>$@,   $(call param-cmt,text,$(1))))\
$(foreach t,$(call param-text,$(1)),\
$(file >>$@,\#if PARAM_$(param.$(t).opt))\
$(file >>$@,    | param_$(t)$(call param-cmt,$(t),$(1)))\
$(file >>$@,\#endif /* PARAM_$(param.$(t).opt) */))\
$(if $(or $(2),$(call param-virt,$(1))),\
$(file >>$@,   $(call param-cmt,flag,$(1))))\
$(foreach f,$(strip $(2) $(if $(call param-virt,$(1)),virtual)),\
$(if $(param.$(f).opt),$(file >>$@,\#if PARAM_$(param.$(f).opt)))\
$(file >>$@,    | param_$(f)$(call param-cmt,$(f),$(1)))\
$(if $(param.$(f).opt),$(file >>$@,\#endif /* PARAM_$(param.$(f).opt) */)))\
$(file >>$@,    ,)\
$(file >>$@,    $(call param.data.size,$(1)),$(call param-cmt,size,$(1)))\
$(file >>$@,    $(call param.data.cell,$(1)),$(call param-cmt,cell,$(1)))\
$(file >>$@,\#if PARAM_CONS)\
$(file >>$@,    $(if $(or $(call param-cons,$(1)),$(filter enum ufix sfix,$(call param-type,$(1)))),$(1)_cons,NULL),$(call param-cmt,cons,$(1)))\
$(file >>$@,\#endif /* PARAM_CONS */)\
$(file >>$@,\#if PARAM_TEXT)\
$(file >>$@,    $(if $(or $(call param-text,$(1)),$(filter enum,$(call param-type,$(1)))),$(1)_text,NULL)$(call param-cmt,text,$(1)))\
$(file >>$@,\#endif /* PARAM_TEXT */)
param.des.def = $(call param.des.def-,$(1),$(call param-has,$(1),$(param.flags)))

param-exec = $(shell $(1))$(call param-log,debug,$(1))
param-req = $(call param-exec,git config --file "$(1)" $(if $(2),--get "$(2)",--list | sed 's/=.*$$//g'))

# load parameter field data: <input> <output> <path>
define -param-data
$(2): param.node$(3) := $$(call param-req,$(1),$(patsubst .%,%,$(3)))
endef

# set parameter field list: <input> <output> <path> <list>
define -param-node
$(2): param.node$(3) := $(4)
endef

# select top level variables <variables>
param-sel = $(call param-uniq,$(foreach n,$(1),$(word 1,$(subst ., ,$(n)))))
# select sub-variables <name> <variables>
param-sub = $(patsubst $(1).%,%,$(filter $(1).%,$(2)))

# <input> <output> <variables> <prefix> <list>
param-tree- = $(if $(filter-out 0,$(words $(5))),\
$(eval $(call -param-node,$(1),$(2),$(4),$(5)))\
$(foreach n,$(5),\
$(call param-tree,$(1),$(2),$(call param-sub,$(n),$(3)),$(4).$(n))),\
$(eval $(call -param-data,$(1),$(2),$(4))))
# build parameter data tree: <input> <output> <variables> [<prefix>]
param-tree = $(call param-tree-,$(1),$(2),$(3),$(4),$(call param-sel,$(3)))

# initialize parameter data: <input> <output>
param-init = $(call param-tree,$(1),$(2),$(call param-req,$(1)))

# remove duplicates without change the order: <list>
param-uniq = $(if $(filter 0,$(words $(1))),,$(word 1,$(1)) $(filter-out $(word 1,$(1)),$(call param-uniq,$(wordlist 2,$(words $(1)),$(1)))))

# get list of parameters: [<type>]
param-list- = $(if $(2),$(foreach p,$(1),$(if $(filter $(2),$(call param-type,$(p))),$(p))),$(1))
param-list = $(call param-list-,$(param.node.param),$(1))

# get list of enum options: <param>
param-vals = $(param.node.value.$(1))

# extend fields
param-lang-add- = $(if $(2),$(foreach f,$(1),$(f)-$(2) $(f)),$(1))
param-lang-add = $(call param-lang-add-,$(1),$(param-lang))

param-lang-del = $(call param-uniq,$(patsubst %-$(param-lang),%,$(1)))

# <prefix> <fields>
param-has-1 = $(filter $(2),$($(1)))$(if $($(1).class), $(foreach c,$($(1).class),$(call param-has-1,param.node.class.$(c),$(2))))
# check parameter field: <param> <fields> [<prefix>]
param-has = $(call param-lang-del,$(call param-has-1,param.node.$(if $(3),value.$(3),param).$(1),$(call param-lang-add,$(2))))

# <prefix> <fields>
param-get-3 = $(if $(2),$(or $($(1).$(word 1,$(2))),$(call param-get-3,$(1),$(wordlist 2,$(words $(2)),$(2)))))
# <prefixes> <fields>
param-get-2 = $(if $(1),$(or $(call param-get-3,$(word 1,$(1)),$(2)),$(call param-get-2,$(wordlist 2,$(words $(1)),$(1)),$(2)),$(if $($(word 1,$(1)).class),$(call param-get-2,$(addprefix param.node.class.,$($(word 1,$(1)).class)),$(2)))))
# <param> <fields> <default> [<prefix>]
param-get-1 = $(if $(2),$(call param-get-2,param.node.$(if $(4),value.$(4),param).$(1),$(call param-lang-add,$(2))),$(3))
# get value of parameter field: <param> <fields> <default> [<prefix>]
param-get = $(call param-get-1,$(1),$(call param-has,$(1),$(2),$(4)),$(3),$(4))

# get item number: <item> <list> (<seen>)
param-num = $(if $(filter $(1),$(word 1,$(2))),$(words $(3)),$(call param-num,$(1),$(wordlist 2,$(words $(2)),$(2)),$(3) $(word 1,$(2))))

# item is last: <item> <list>
param-end = $(filter $(call param-num,$(1),_ $(2)),$(words $(2)))

# optional separator: <item> <list> [<sep>]
param-sep = $(if $(call param-end,$(1),$(2)),,$(or $(3),$(strip ,)))

# get comment: <key> <arg1> <arg2> ...
param-cmt = $(if $(param.$(1).cmt), /* $(call param.$(1).cmt,$(2),$(3),$(4),$(5)) */)

# check the parameter definition <param>
param-check = $(if $(filter-out $(param.types),$(call param-type,$(1))),$(error Unsupported type '$(call param-type,$(1))' of parameter '$(1)'!))

# get type of parameter: <param>
param-type = $(call param-get,$(1),type,none)
# get cons fields of parameter: <param>
param-cons = $(call param-has,$(1),$(call param.$(call param-type,$(1)).cons.fields))
# get text fields of parameter: <param>
param-text = $(call param-has,$(1),$(param.text.fields))
# the parameter is group: <param>
param-grp = $(filter none,$(call param-type,$(1)))
# the parameter has no associated value: <param>
param-null = $(if $(call param-has,$(1),getable setable),$(if $(call param-virt,$(1)),$(if $(call param-has,$(1),data),,null),),null)
param-void = $(if $(call param-has,$(1),getable setable),,null)
# get size of parameter: <param>
param-size = $(if $(call param-grp,$(1)),0,$(call param-get,$(1),size))
# get bind of parameter: <param>
param-bind = $(if $(or $(call param-grp,$(1)),$(call param-void,$(1))),,$(if $(call param-has,$(1),bind),$(or $(call param-get,$(1),bind),$(1))))
# get data name of parameter: <param>
param-data = $(if $(or $(call param-grp,$(1)),$(call param-null,$(1)),$(call param-bind,$(1))),,$(or $(call param-get,$(1),name),$(1)))
# parameter is virtual: <param>
param-virt = $(call param-has,$(1),get set)

# wrap param using condition
param-open- = $(if $(1),$(file >>$@,\#if $(1)))
param-open = $(call param-open-,$(call param-get,$(1),cond))
param-close- = $(if $(1),$(file >>$@,\#endif /* $(1) */))
param-close = $(call param-close-,$(call param-get,$(1),cond))

# <input> <output> <namespace> <language> <depends...>
define -param-make
$(call param-init,$(1),$(2))
$(2): param-src := $(1)
# parameter namespace
$(2): param-ns := $(3)
# target locale language
$(2): param-lang := $(4)
# the rules
$(2): $(1) $(dir $(2))
	@echo PROC PARAMS $$@ FROM $$<
	$$(foreach p,$$(param-list),\
	$$(call param-check,$$(p)))
	$$(file >$$@,/**)
	$$(file >>$$@, * This file is generated from $$<)
	$$(file >>$$@, * Usually you dont need to edit it manually.)
	$$(file >>$$@, */)
	$$(file >>$$@,)
	$$(file >>$$@,#ifndef __$$(param-ns)_h__)
	$$(file >>$$@,#define __$$(param-ns)_h__)
	$$(file >>$$@,)
	$$(file >>$$@,#ifndef __PARAM_H__)
	$$(file >>$$@,#include "param.h")
	$$(file >>$$@,#endif /* __PARAM_H__ */)
	$$(file >>$$@,)
	$$(file >>$$@,extern const param_coll_t $$(param-ns);$$(call param-cmt,coll,$$(param-ns)))
	$$(file >>$$@,)
# Define references to param descriptors
	$$(file >>$$@,typedef union { /* parameters collection */)
	$$(file >>$$@,  struct { /* parameter by name */)
	$$(foreach p,$$(param-list),\
	$$(call param-open,$$(p))\
	$$(file >>$$@,    param_desc_t $$(p);)\
	$$(call param-close,$$(p)))
	$$(file >>$$@,  } by_name;)
	$$(file >>$$@,  param_desc_t by_num[0]; /* parameter by number */)
	$$(file >>$$@,} $$(param-ns)_t;)
	$$(file >>$$@,)
	$$(file >>$$@,#define $$(param-ns)_ptr(name) (&(((const $$(param-ns)_t*)$$(param-ns).param)->by_name.name)))
	$$(file >>$$@,)
	$$(foreach t,$$(param.types),$$(call param.$$(t).type.def))
	$$(file >>$$@,)
	$$(foreach p,$$(param-list),\
	$$(call param-open,$$(p))\
	$$(call param.data.def,$$(p))\
	$$(call param-close,$$(p)))
	$$(file >>$$@,)
	$$(file >>$$@,#endif /* __$$(param-ns)_h__ */)
	$$(file >>$$@,)
	$$(file >>$$@,#ifdef __$$(param-ns)_c__)
	$$(file >>$$@,)
# Declare param definitions
	$$(foreach p,$$(param-list),\
	$$(call param-open,$$(p))\
	$$(call param.cons.def,$$(p))\
	$$(call param.text.def,$$(p))\
	$$(call param.data.imp,$$(p))\
	$$(call param.vmt.def,$$(p))\
	$$(call param-close,$$(p)))
	$$(file >>$$@,)
# Declare array of param descriptors
	$$(file >>$$@,static const param_desc_t $$(param-ns)_list[] PARAM_ROM_ATTR = {)
	$$(foreach p,$$(param-list),\
	$$(call param-open,$$(p))\
	$$(file >>$$@,  {$$(call param-cmt,des,$$(p)))\
	$$(call param.des.def,$$(p))\
	$$(file >>$$@,  }$$(call param-sep,$$(p),$$(param-list)))\
	$$(call param-close,$$(p)))
	$$(file >>$$@,};)
	$$(file >>$$@,)
	$$(file >>$$@,const param_coll_t $$(param-ns) PARAM_ROM_ATTR = {)
	$$(file >>$$@,  sizeof($$(param-ns)_list)/sizeof($$(param-ns)_list[0]),)
	$$(file >>$$@,  $$(param-ns)_list)
	$$(file >>$$@,};)
	$$(file >>$$@,)
	$$(file >>$$@,#endif /* __$$(param-ns)_c__ */)
ifneq (,$$(strip $(5)))
$$(strip $(5)): $(2)
endif
clean.param: clean.param.$(3)
clean.param.$(3):
	@echo CLEAN PARAMS $(3)
	$(Q)rm -f $(2)
endef

clean: clean.param

# Generate parameters definition: <input> <output> <namespace> <language> <depends>
param-make = $(eval $(call -param-make,$(1),$(2),$(or $(3),$(basename $(notdir $(2)))),$(4),$(5)))
